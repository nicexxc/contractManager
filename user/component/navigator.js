var nickName = $.cookie('nickname');
var role = $.cookie('role');
var unread = $.cookie('unread');
var judgeAdmin = authJudge(['manager','financialOfficer','cheif']);
var judgeSideShowH = authJudge(['manager','financialOfficer','cheif','areaCheif','operator']);
var judgeSideShow = authJudge(['manager','financialOfficer','cheif','areaCheif','purchaser','operator']);
// 导航栏的头部
Vue.component('navigator-header', {
    template: '<div class="layui-header">\n' +
        '        <div class="layui-logo">合同管理系统</div>\n' +
        '        <!-- 头部区域（可配合layui已有的水平导航） -->\n' +
        '        <ul class="layui-nav layui-layout-right">\n' +
        '            <li class="layui-nav-item">\n' +
        '                <a href="javascript:;">\n' +
        '                    '+(nickName?nickName:'未知用户')+'\n' +
        '                </a>\n' +
        '                <dl class="layui-nav-child">\n' +
        '                    <dd><a href="./private.html" target="_blank">安全设置</a></dd>\n' +
        (judgeAdmin(role) ? '<dd><a href="'+IP_+'admin/html/staff.html">管理端</a></dd>' : '')+
        '                </dl>\n' +
        '            </li>\n' +
        '            <li class="layui-nav-item" @click="logout"><a>退出登录</a></li>\n' +
        '        </ul>\n' +
        '    </div>\n' +
        '\n',
    methods:{
        logout:function(){
            this.$Modal.confirm({
                title:'确定要退出登录吗？',
                onOk:function () {
                    $.ajax({
                        type:'get',
                        url:IP+'logout',
                        data:{},
                        success:function (res) {
                            if(res.code == 200){
                                window.location.href = IP_ + 'login.html'
                            }
                        }
                    });
                }
            })
        }
    }
})
// 导航栏的侧边栏
Vue.component('navigator-side',{
    props:['page'],
    template: '\n' +
        '  <div class="layui-side layui-bg-black">\n' +
        '    <div class="layui-side-scroll">\n' +
        '      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->\n' +
        '      <ul class="layui-nav layui-nav-tree"  lay-filter="test">\n' +
        '        <li :class="page1" @click="choiseTab">\n' +
        '          <a href="./contractList.html">合同管理</a>\n' +
        '        </li>\n' +
        (judgeSideShowH(role) ? '<li :class="page2" @click="choiseTab"><a href="./financeList.html">财务管理</a></li>\n':'') +
        (judgeAdmin(role) ? '<li :class="page3" @click="choiseTab"><a href="./fileContractList.html">合同归档</a></li>\n':'') +
        (judgeSideShow(role) ? '<li :class="page4" @click="choiseTab"><a href="./messagelist.html">消息列表'+(unread != 0 ? '<span class="layui-badge" id="unread-bubble">'+unread+'</span>' : '')+'</a></li>\n':'') +
        '      </ul>\n' +
        '    </div>\n' +
        '  </div>',
    methods: {
        choiseTab:function (e) {
            $('.layui-nav-item').removeClass('layui-this');
            $(e.target).parent().addClass('layui-this');
        }
    },
    computed: {
        page1:function(){
            if(this.page == 1){
                return "layui-nav-item layui-this"
            }else{
                return "layui-nav-item"
            }
        },
        page2:function(){
            if(this.page == 2){
                return "layui-nav-item layui-this"
            }else{
                return "layui-nav-item"
            }
        },
        page3:function(){
            if(this.page == 3){
                return "layui-nav-item layui-this"
            }else{
                return "layui-nav-item"
            }
        },
        page4:function(){
            if(this.page == 4){
                return "layui-nav-item layui-this"
            }else{
                return "layui-nav-item"
            }
        },
    },
})
// 导航栏的底部区域
Vue.component('navigator-footer',{
    template:'<div class="layui-footer">\n' +
        '        <!-- 底部固定区域 -->\n' +
        '        ©浙江工商大学ITObase\n' +
        '    </div>'
})
var navigator = new Vue({
    el:'#navigator',
    data:{
        page:0
    }
});