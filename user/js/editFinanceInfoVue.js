(function (global) {
    var body = new Vue({
        el: '#finance-info',
        data: {
            /**
             * 从url中取出
             */
            areaCheif:true,
            tag: 0, //合同类型标记
            id: 0, //合同id
            tab: 0, //指定跳转的选项卡
            /**
             * 5个tab共用
             */
            contractName: '', //合同名
            contractSum: 0, //合同总额
            /**
             * 应收款
             */
            receiveId: 0, //更新时使用，应收款条目id
            receiveStage: 0, //更新时使用，应收款条目期数
            receiveModal: false, //普通合同弹出框
            receiveMoney: 0, //普通合同金额
            receiveDate: '', //普通合同日期
            receiveRemark: '', //普通合同备注
            receiveModalY: false, //运维合同弹出框
            receiveTypeY: 0, //运维合同类型
            receiveMoneyY: 0, //运维合同金额
            receiveEventY: 0,
            receiveDateY: '', //运维合同日期
            receiveRemarkY: '', //运维合同备注
            receiveDaysY: 0, //运维合同天数
            receiveYearsY: 0, //运维合同年数
            nodeType: 0, //选择的节点ID
            nodeList: [], //节点列表
            receivables: 0, //节点应收款
            receiveRemainer: 0, //节点应收款余额
            receivablesList: [], //应收款项
            /**
             * 应开票
             */
            billId: 0,
            billModal: false,
            billRemainer: 0, //剩余未开票余额
            billList: [], //应开票项
            billMoney: 0,
            billDate: '',
            billRemark: '',
            /**
             * 实收款
             */
            salerList: [], //销售员列表
            salerSelectedList: [], //被选中的销售员列表
            receiveActualMoney: 0, //实收款
            receiveActualDate: '', //收款日期
            receiveActualRemark: '', //备注
            receiveActualPayDate: '', //应支付时间
            receiveActualModal: false,
            actualReceiveRemainer: 0, //待收款余额
            actualReceivablesList: [], //实收款项
            /**
             * 实开票
             */
            actualBillMoney: 0,
            actualBillDate: '',
            actualBillRemark: '',
            actualBillModal: false,
            actualBillRemainer: 0, //剩余应开票余额
            actualBillList: [], //实开票项
        },
        beforeMount() {
            var that = this;
            var params = this.getParams();
            var role = $.cookie('role');
            var judgeArea = authJudge(['areaCheif','operator']);
            this.areaCheif = judgeArea(role);
            this.id = params.id;
            this.tag = params.tag;
            this.tab = params.tab;
            this.contractName = decodeURI(params.name);
            this.changeTab();
            $.ajax({
                type: 'GET',
                url: IP + "staff/saler/" + this.id,
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        var list = res.data;
                        for (var i = 0; i < list.length; i++) {
                            var saler = list[i];
                            that.salerList.push({
                                seller_name: saler.name,
                                seller_id: saler.id,
                                percentage: 0
                            });
                        }
                    } else {
                        that.$Message.error({
                            content: '请求销售员列表失败',
                            duration: 6
                        });
                    }
                },
                error: function () {
                    that.$Message.error({
                        content: '请求销售员列表失败',
                        duration: 6
                    });
                }
            });
        },
        methods: {
            /**
             * 切换页面
             * @param {number} tab 页面编号0~3
             */
            changeTab: function (tab) {
                if (tab != undefined) {
                    this.tab = tab;
                }
                if (this.tab == 0) {
                    if (this.tag == 0) {
                        this.getReceiveInfo();
                    } else {
                        this.getReceiveTList();
                    }
                } else if (this.tab == 1) {
                    this.getBillList();
                } else if (this.tab == 2) {
                    this.getActualReceiveList();
                } else if (this.tab == 3) {
                    this.getActualBillList();
                }
            },
            /**
             * 时间转换
             * @param {timestamp} ts js默认时间格式
             */
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                    return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
                }
            },
            /**
             * 获取url中的属性
             */
            getParams: function () {
                var url = document.location.toString();
                var params = url.split('?')[1].split('&');
                var id = params[0].split('=')[1];
                var tag = params[1].split('=')[1];
                var tab = params[2].split('=')[1];
                var name = params[3].split('=')[1];
                return {
                    id: id,
                    tag: tag,
                    tab: tab,
                    name: name
                }
            },
            /**
             * 点击右下角按钮
             */
            addItem: function () {
                var tab = this.tab;
                var tag = this.tag;
                if (tab == 0) { //应收款
                    if (tag == 0) { //普通合同
                        if (this.nodeType != 0) {
                            this.receiveModal = true;
                        } else {
                            this.$Message.error({
                                content: '请先选择节点',
                                duration: 6
                            });
                        }
                    } else { //运维合同
                        this.receiveModalY = true;
                    }
                } else if (tab == 1) { //应开票
                    this.billModal = true;
                } else if (tab == 2) { //实收款
                    this.receiveActualModal = true;
                } else if (tab == 3) { //实开票
                    this.actualBillModal = true;
                }
            },
            /**
             * 获取应收款信息——普通
             */
            getReceiveInfo() {
                var that = this;
                if (this.tag == 0) {
                    $.ajax({
                        type: 'GET',
                        url: IP + "contract/money",
                        data: {
                            contractId: that.id
                        },
                        success: function (res) {
                            if (res.code == 200) {
                                that.contractSum = res.data.amount;
                                that.nodeList = res.data.contractNode;
                                // that.receiveRemainer = res.data.restMoneyActual;
                            } else {
                                that.$Message.error({
                                    content: '请求失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '请求失败',
                                duration: 6
                            });
                        }
                    });
                }
            },
            /**
             * 运维合同节点判断
             * @param {string} str 
             */
            name2code: function (str) {
                var code = 0;
                if(str == '服务开始'){
                    code = 1;
                }else if(str == '合同签订'){
                    code = 2;
                }else if(str == '服务结束'){
                    code = 3;
                }
                return code;
            },
            /**
             * 获取应收款列表——普通运维通用
             */
            getReceiveTList: function () {
                var that = this;
                if (this.tag == 0) { //获取普通类别合同应收款列表
                    $.ajax({
                        type: 'GET',
                        url: IP + '/contractNode/' + that.nodeType,
                        data: {},
                        success: function (res) {
                            var d = res.data;
                            if (res.code == 200) {
                                that.receivables = d.nodeMoney;
                                that.receiveRemainer = d.restNodeMoney;
                                that.receivablesList = d.receivableRecord;
                            } else {
                                that.$Message.error({
                                    content: '获取列表失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '获取列表失败',
                                duration: 6
                            });
                        }
                    });
                } else {
                    $.ajax({
                        type: 'GET',
                        url: IP + 'operationAccountRe',
                        data: {
                            contractId: that.id
                        },
                        success: function (res) {
                            var d = res.data;
                            if (res.code == 200) {
                                that.contractSum = d.contractMoney;
                                that.receiveRemainer = d.restMoney;
                                that.receivablesList = d.operationReList;
                            } else {
                                that.$Message.error({
                                    content: '获取列表失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '获取列表失败',
                                duration: 6
                            });
                        }
                    });
                    this.getReceiveInfo();
                }
            },
            /**
             * 更新、添加应收款——普通
             */
            updateReceive: function () {
                var that = this;
                if (this.receiveId == 0) { //添加
                    var list = this.receivablesList;
                    var moneySum = 0;
                    for (var i = 0; i < list.length; i++) {
                        moneySum += list[i].money;
                    }
                    var data = {
                        contractId: parseInt(this.id),
                        endDate: this.changeTS2ST(this.receiveDate),
                        money: this.receiveMoney,
                        nodeId: this.nodeType,
                        remark: this.receiveRemark,
                        stage: this.receivablesList.length + 1
                    }
                    if (this.receiveMoney == 0) {
                        that.$Message.error({
                            content: '请填写金额',
                            duration: 6
                        });
                    } else if (this.receiveDate == '') {
                        that.$Message.error({
                            content: '请填写日期',
                            duration: 6
                        });
                    } else if (this.receiveMoney > (this.receivables - moneySum)) {
                        that.$Message.error({
                            content: '金额过大',
                            duration: 6
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: IP + "commonAccountRe",
                            contentType: 'application/json',
                            data: JSON.stringify(data),
                            success: function (res) {
                                if (res.code == 200) {
                                    that.$Message.success({
                                        content: '添加成功',
                                        duration: 6
                                    });
                                    that.getReceiveTList();
                                } else if (res.code == 410) {
                                    that.$Message.error({
                                        content: res.msg,
                                        duration: 6
                                    });
                                } else {
                                    that.$Message.error({
                                        content: '添加失败',
                                        duration: 6
                                    });
                                }
                            },
                            error: function () {
                                that.$Message.error({
                                    content: '添加失败',
                                    duration: 6
                                });
                            }
                        });
                        this.cancelReceive();
                    }
                } else { //更新 undo校验 discard
                    var data = {
                        id: this.receiveId,
                        contractId: parseInt(this.id),
                        endDate: this.changeTS2ST(this.receiveDate),
                        money: this.receiveMoney,
                        nodeId: this.nodeType,
                        remark: this.receiveRemark,
                        stage: this.receiveStage
                    }
                    $.ajax({
                        type: 'PUT',
                        url: IP + "commonAccountRe",
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function (res) {
                            if (res.code == 200) {
                                that.$Message.success({
                                    content: '更新成功',
                                    duration: 6
                                });
                                that.getReceiveTList();
                            } else if (res.code == 410) {
                                that.$Message.error({
                                    content: res.msg,
                                    duration: 6
                                });
                            } else {
                                that.$Message.error({
                                    content: '更新失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '更新失败',
                                duration: 6
                            });
                        }
                    });
                    this.receiveStage = 0;
                    this.receiveId = 0;
                }
            },
            /**
             * @discard
             * 更新应收款——普通
             * @param {number} id 合同id
             * @param {number} index 所处列表的下标
             */
            changeReceive: function (id, index) {
                var item = this.receivablesList[index];
                //设置本项应收款内容到编辑框内
                this.receiveMoney = item.money;
                this.receiveDate = item.date;
                this.receiveRemark = item.remark;
                this.receiveId = id;
                this.receiveStage = index + 1;

                this.receiveModal = true; //显示普通合同编辑框
            },
            /**
             * 应收款modal归零——普通
             */
            cancelReceive: function () {
                this.receiveMoney = 0;
                this.receiveDate = '';
                this.receiveRemark = '';
            },
            /**
             * 添加应收款——运维
             */
            updateReceiveY: function () {
                var that = this;
                var receiveTypeY = this.receiveTypeY;
                var list = this.receivablesList;
                var moneySum = 0;
                for (var i = 0; i < list.length; i++) {
                    // if (list[i].nodeInfo.type == 5) {
                    //     moneySum += list[i].money * list[i].nodeInfo.years;
                    // } else {
                    moneySum += list[i].money;
                    // }
                }
                var data = {
                    contractId: parseInt(this.id),
                    money: this.receiveMoneyY,
                    remark: this.receiveRemarkY,
                    type: this.receiveTypeY,
                    days: this.receiveDaysY,
                    years: this.receiveYearsY
                }
                if (receiveTypeY == 2 || receiveTypeY == 4) {
                    var t = '';
                    if (this.receiveEventY == 1) {
                        t = '服务开始'
                    } else if (this.receiveEventY == 2) {
                        t = '合同签订'
                    } else if (this.receiveEventY == 3) {
                        t = '服务结束'
                    }
                    data.time = t;
                } else if (receiveTypeY == 1 || receiveTypeY == 3 || receiveTypeY == 5) {
                    data.time = this.changeTS2ST(this.receiveDateY);
                }

                if (this.receiveTypeY == 0) {
                    this.$Message.error({
                        content: '请选择应收款类型',
                        duration: 6
                    });
                } else if ((receiveTypeY == 1 || receiveTypeY == 3 || receiveTypeY == 5) && this.receiveDateY == '') {
                    this.$Message.error({
                        content: '请选择日期',
                        duration: 6
                    });
                } else if ((receiveTypeY == 1 || receiveTypeY == 2 || receiveTypeY == 5) && this.receiveDaysY == 0) {
                    this.$Message.error({
                        content: '请填写工作日',
                        duration: 6
                    });
                } else if ((receiveTypeY == 2 || receiveTypeY == 4) && this.receiveEventY == 0) {
                    this.$Message.error({
                        content: '请选择事件类型',
                        duration: 6
                    });
                } else if (receiveTypeY == 5 && this.receiveYearsY == 0) {
                    this.$Message.error({
                        content: '请填写年数',
                        duration: 6
                    });
                } else if (this.receiveMoneyY == 0) {
                    this.$Message.error({
                        content: '请填写金额',
                        duration: 6
                    });
                } else if (this.receiveMoneyY > (this.contractSum - moneySum)) {
                    this.$Message.error({
                        content: '金额过大',
                        duration: 6
                    });
                } else {
                    $.ajax({
                        type: 'POST',
                        url: IP + "operationAccountRe",
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function (res) {
                            if (res.code == 200) {
                                that.$Message.success({
                                    content: '添加成功',
                                    duration: 6
                                });
                                that.getReceiveTList();
                            } else if (res.code == 410 || res.code == 505) {
                                that.$Message.error({
                                    content: res.msg,
                                    duration: 6
                                });
                            } else {
                                that.$Message.error({
                                    content: '添加失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '添加失败',
                                duration: 6
                            });
                        }
                    });
                    this.cancelReceiveY();
                }
            },
            /**
             * @discard
             * 更新应收款——运维
             */
            changeReceiveY: function () {

            },
            /**
             * 应收款modal归零——运维
             */
            cancelReceiveY: function () {
                this.receiveMoneyY = 0;
                this.receiveRemarkY = '';
                this.receiveTypeY = 0;
                this.receiveDateY = '';
                this.receiveDaysY = 0;
                this.receiveYearsY = 0;
            },
            /**
             * 删除应收款——普通运维通用
             * @param {number} id 应收款条目id
             */
            deleteReceiveT: function (id) {
                var that = this;
                this.$Modal.confirm({
                    title: '确定删除本项应收款吗？',
                    onOk: function () {
                        if (that.tag == 0) { //普通合同
                            $.ajax({
                                type: 'DELETE',
                                url: IP + 'commonAccountRe?accountReId=' + id + '&nodeId=' + that.nodeType,
                                data: {},
                                success: function (res) {
                                    if (res.code == 200) {
                                        that.getReceiveTList();
                                        that.$Message.success({
                                            content: '删除成功',
                                            duration: 6
                                        });
                                    } else if (res.code == 510) {
                                        that.$Message.error(res.msg);
                                    } else {
                                        that.$Message.error({
                                            content: '删除失败',
                                            duration: 6
                                        });
                                    }
                                },
                                error: function () {
                                    that.$Message.error({
                                        content: '删除失败',
                                        duration: 6
                                    });
                                }
                            });
                        } else { //运维合同
                            $.ajax({
                                type: 'DELETE',
                                url: IP + 'operationAccountRe?nodeId=' + id,
                                data: {},
                                success: function (res) {
                                    if (res.code == 200) {
                                        that.$Message.success({
                                            content: '删除成功',
                                            duration: 6
                                        });
                                        that.getReceiveTList();
                                    } else if (res.code == 510) {
                                        that.$Message.error({
                                            content: res.msg,
                                            duration: 6
                                        });
                                    } else {
                                        that.$Message.error({
                                            content: '删除失败',
                                            duration: 6
                                        });
                                    }
                                },
                                error: function () {
                                    that.$Message.error({
                                        content: '删除失败',
                                        duration: 6
                                    });
                                }
                            });
                        }
                    }
                });
            },
            /**
             * 获取应开票列表
             */
            getBillList: function () {
                var that = this;
                $.ajax({
                    type: 'GET',
                    url: IP + "invoiceReceivable",
                    data: {
                        contractId: that.id
                    },
                    success: function (res) {
                        var d = res.data;
                        if (res.code == 200) {
                            that.contractSum = d.contractAmount;
                            that.billRemainer = d.resetInvoiceMoney;
                            that.billList = d.invoiceList;
                        } else {
                            that.$Message.error({
                                content: '获取列表失败',
                                duration: 6
                            });
                        }
                    },
                    error: function () {
                        that.$Message.error({
                            content: '获取列表失败',
                            duration: 6
                        });
                    }
                });
            },
            /**
             * 添加、修改应开票
             */
            updateBill: function () {
                var that = this;
                if (this.billId == 0) {
                    var list = this.billList;
                    var moneySum = 0;
                    for (var i = 0; i < list.length; i++) {
                        moneySum += list[i].invoice_money;
                    }
                    var data = {
                        contractId: parseInt(this.id),
                        date: this.changeTS2ST(this.billDate),
                        invoiceMoney: this.billMoney,
                        remark: this.billRemark
                    }
                    if (this.billMoney == 0) {
                        this.$Message.error({
                            content: '请填写金额',
                            duration: 6
                        });
                    } else if (this.billDate == '') {
                        this.$Message.error({
                            content: '请选择日期',
                            duration: 6
                        });
                    } else if (this.billMoney > (this.contractSum - moneySum)) {
                        this.$Message.error({
                            content: '金额过大',
                            duration: 6
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: IP + "invoiceReceivable",
                            contentType: 'application/json',
                            data: JSON.stringify(data),
                            success: function (res) {
                                if (res.code == 200) {
                                    that.getBillList();
                                    that.$Message.success({
                                        content: '添加成功',
                                        duration: 6
                                    });
                                } else if (res.code == 410) {
                                    that.$Message.error({
                                        content: res.msg,
                                        duration: 6
                                    });
                                } else {
                                    that.$Message.error({
                                        content: '添加失败',
                                        duration: 6
                                    });
                                }
                            },
                            error: function () {
                                that.$Message.error({
                                    content: '添加失败',
                                    duration: 6
                                });
                            }
                        });
                        this.cancelBill();
                    }

                } else {
                    var data = {
                        id: this.billId,
                        contractId: parseInt(this.id),
                        date: this.changeTS2ST(this.billDate),
                        invoiceMoney: this.billMoney,
                        remark: this.billRemark
                    }
                    $.ajax({
                        type: 'PUT',
                        url: IP + "invoiceReceivable",
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function (res) {
                            if (res.code == 200) {
                                that.getBillList();
                                that.$Message.success({
                                    content: '更新成功',
                                    duration: 6
                                });
                            } else if (res.code == 410) {
                                that.$Message.error({
                                    content: res.msg,
                                    duration: 6
                                });
                            } else {
                                that.$Message.error({
                                    content: '更新失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '更新失败',
                                duration: 6
                            });
                        }
                    });
                }
            },
            /**
             * @discard
             * 更新应开票
             * @param {number} id 应开票条目id
             */
            changeBill: function (id, index) {
                var item = this.billList[index];

                this.billMoney = item.invoice_money;
                this.billDate = item.date;
                this.billRemark = item.remark;
                this.billId = id;

                this.billModal = true;
            },
            /**
             * 清空应开票modal
             */
            cancelBill: function () {
                this.billMoney = 0;
                this.billDate = '';
                this.billRemark = '';
                this.billId = 0;
            },
            /**
             * 删除应开票
             */
            deleteBill: function (id) {
                var that = this;
                this.$Modal.confirm({
                    title: '确定删除本项应开票吗？',
                    onOk: function () {
                        $.ajax({
                            type: 'DELETE',
                            url: IP + 'invoiceReceivable?invoiceReId=' + id,
                            data: {},
                            success: function (res) {
                                if (res.code == 200) {
                                    that.getBillList();
                                    that.$Message.success({
                                        content: '删除成功',
                                        duration: 6
                                    });
                                } else if (res.code == 510) {
                                    that.$Message.error({
                                        content: res.msg,
                                        duration: 6
                                    });
                                } else {
                                    that.$Message.error({
                                        content: '删除失败',
                                        duration: 6
                                    });
                                }
                            },
                            error: function () {
                                that.$Message.error({
                                    content: '删除失败',
                                    duration: 6
                                });
                            }
                        });
                    }
                });
            },
            /**
             * 获取实收列表
             */
            getActualReceiveList: function () {
                var that = this;
                $.ajax({
                    type: 'GET',
                    url: IP + "api/v1/actural/get_receipt_list",
                    data: {
                        contract_id: that.id
                    },
                    success: function (res) {
                        if (res.code == 200) {
                            that.actualReceivablesList = res.data.acture_receipts;
                            that.contractSum = res.data.total_amount;
                            var list = that.actualReceivablesList;
                            var moneySum = 0;
                            for (var i = 0; i < list.length; i++) {
                                moneySum += list[i].receipt;
                            }
                            that.actualReceiveRemainer = that.contractSum - moneySum;
                        } else if (res.code == -1) {
                            that.$Message.error({
                                content: res.msg,
                                duration: 6
                            });
                        } else {
                            that.$Message.error({
                                content: '获取列表失败',
                                duration: 6
                            });
                        }
                    },
                    error: function () {
                        that.$Message.error({
                            content: '获取列表失败',
                            duration: 6
                        });
                    }
                });
            },
            /**
             * 添加实收
             */
            addActualReceive: function () {
                var that = this;
                var selectedIdList = this.salerSelectedList;
                var salerList = this.salerList;
                var sendList = [];
                for (var i = 0; i < selectedIdList.length; i++) {
                    var selectedId = selectedIdList[i];
                    for (var j = 0; j < salerList.length; j++) {
                        if (salerList[j].seller_id == selectedId) {
                            sendList.push(salerList[j]);
                        }
                    }
                }
                var list = this.actualReceivablesList;
                var moneySum = 0;
                for (var i = 0; i < list.length; i++) {
                    moneySum += list[i].receipt;
                }
                var data = {
                    contract_id: this.id,
                    due_date: this.changeTS2ST(this.receiveActualPayDate),
                    receipt: this.receiveActualMoney,
                    receipt_date: this.changeTS2ST(this.receiveActualDate),
                    remark: this.receiveActualRemark,
                    sellers: sendList
                }

                function judgeMoney(){
                    var judge = false;
                    var itemAccount = parseInt(that.receiveActualMoney);
                    sendList.forEach(function (e) {
                        console.log((parseInt(e.percentage))*itemAccount);
                        if(((parseInt(e.percentage)/100)*itemAccount)<1){
                            judge = true;
                        }
                    });
                    return judge;
                }

                if (this.receiveActualMoney == 0) {
                    that.$Message.error({
                        content: '请填写金额',
                        duration: 6
                    });
                } else if (this.receiveActualDate == '') {
                    that.$Message.error({
                        content: '请选择收款日期',
                        duration: 6
                    });
                } else if (this.receiveActualMoney > (this.contractSum - moneySum)) {
                    that.$Message.error({
                        content: '金额过大',
                        duration: 6
                    });
                } else if (judgeMoney()) {
                    that.$Message.error({
                        content: '提成金额过小',
                        duration: 6
                    });
                } else {
                    $.ajax({
                        type: 'POST',
                        url: IP + "api/v1/actural/add_receipt",
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function (res) {
                            if (res.code == 200) {
                                that.getActualReceiveList();
                                that.$Message.success({
                                    content: '添加成功',
                                    duration: 6
                                });
                            } else if (res.code == -1) {
                                that.$Message.error({
                                    content: res.msg,
                                    duration: 6
                                });
                            } else {
                                that.$Message.error({
                                    content: '添加失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '添加失败',
                                duration: 6
                            });
                        }
                    });
                    this.cancelActualReceive();
                }
            },
            /**
             * 清空实收modal
             */
            cancelActualReceive: function () {
                this.receiveActualPayDate = '';
                this.receiveActualMoney = 0;
                this.receiveActualDate = '';
                this.receiveActualRemark = '';
                this.salerSelectedList = [];
                var salerList = this.salerList;
                for (var i = 0; i < salerList.length; i++) {
                    salerList[i].percentage = 0;
                }
            },
            /**
             * 获取实开票列表
             */
            getActualBillList: function () {
                var that = this;
                $.ajax({
                    type: 'GET',
                    url: IP + "api/v1/actural/get_actural_ticket",
                    data: {
                        contract_id: that.id
                    },
                    success: function (res) {
                        if (res.code == 200) {
                            that.actualBillList = res.data;
                            var list = that.actualBillList;
                            var moneySum = 0;
                            for (var i = 0; i < list.length; i++) {
                                moneySum += list[i].amount;
                            }
                            that.actualBillRemainer = that.contractSum - moneySum;
                        } else if (res.code == -1) {
                            that.$Message.error({
                                content: res.msg,
                                duration: 6
                            });
                        } else {
                            that.$Message.error({
                                content: '获取列表失败',
                                duration: 6
                            });
                        }
                    },
                    error: function () {
                        that.$Message.error({
                            content: '获取列表失败',
                            duration: 6
                        });
                    }
                });
            },
            /**
             * 添加实开票
             */
            addActualBill: function () {
                var that = this;
                var data = {
                    amount: this.actualBillMoney,
                    contract_id: this.id,
                    date: this.changeTS2ST(this.actualBillDate),
                    remark: this.actualBillRemark
                }
                var list = that.actualBillList;
                var moneySum = 0;
                for (var i = 0; i < list.length; i++) {
                    moneySum += list[i].amount;
                }
                if(this.actualBillMoney == 0){
                    that.$Message.error({
                        content: '请填写金额',
                        duration: 6
                    });
                }else if(this.actualBillDate == ''){
                    that.$Message.error({
                        content: '请选择开票日期',
                        duration: 6
                    });
                }else if(this.actualBillMoney > (this.contractSum - moneySum)){
                    that.$Message.error({
                        content: '金额过大',
                        duration: 6
                    });
                }else{
                    $.ajax({
                        type: 'POST',
                        url: IP + "api/v1/actural/add_actural_ticket",
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function (res) {
                            if (res.code == 200) {
                                that.getActualBillList();
                                that.$Message.success({
                                    content: '添加成功',
                                    duration: 6
                                });
                            } else if (res.code == -1) {
                                that.$Message.error({
                                    content: res.msg,
                                    duration: 6
                                });
                            } else {
                                that.$Message.error({
                                    content: '添加失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '添加失败',
                                duration: 6
                            });
                        }
                    });
                    this.cancelActualBill();
                }
            },
            /**
             * 清空实开票modal
             */
            cancelActualBill: function () {
                this.actualBillMoney = 0;
                this.actualBillDate = '';
                this.actualBillRemark = '';
            }
        },
    });
    global.body = body;
})(window);