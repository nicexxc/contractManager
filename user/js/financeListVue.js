(function (global) {
    var searchDate = new Vue({
        el: '#search-date',
        data: {
            start: '',
            end: ''
        },
        methods: {
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                    return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
                }
            },
            flash: function () {
                $('#search-btn').click();
            }
        }
    });
    //向外暴露
    global.searchDate = searchDate;

    var percentageShow = new Vue({
        el: '#tab-controller',
        data: {
            percentageShow: false
        },
        beforeMount() {
            var role = $.cookie('role');
            var judgeOperator = authJudge(['operator']);
            this.percentageShow = !judgeOperator(role);
        },
        methods: {
            clear: function () {
                exportModal.contractList = new Set();
            }
        }
    });
    global.percentageShow = percentageShow;

    var percentageModal = new Vue({
        el: '#percentage-modal',
        data: {
            show: false,

            id: 0,
            contractSum: 0,
            salerName: '',
            percentage: 0,
            commission: 0,

            date: '',
            remark: '',
        },
        methods: {
            /**
             * 时间转换
             * @param {timestamp} ts js默认时间格式
             */
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                    return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
                }
            },
            showModal: function (id) {
                var that = this;
                $.ajax({
                    url: IP + 'commission/' + id,
                    type: 'GET',
                    data: {},
                    success: function (res) {
                        if (res.code == 200) {
                            var d = res.data;
                            that.id = id;
                            that.contractSum = d.actualMoney;
                            that.salerName = d.salerName;
                            that.percentage = d.percentage;
                            that.commission = d.commission;
                        } else {
                            that.$Message.error({
                                content: '获取提成信息失败',
                                duration: 6
                            });
                        }
                    },
                    error: function (res) {
                        console.log(res);
                        that.$Message.error({
                            content: '获取提成信息失败',
                            duration: 6
                        });
                    }
                });
                this.show = true;
            },
            addPersentage: function () {
                var that = this;
                var data = {
                    id: this.id,
                    finishDate: this.changeTS2ST(this.date),
                    remark: this.remark
                }
                if(data.finishDate == ''&&data.finishDate == null){
                    that.$Message.error({
                        content: '请选择日期',
                        duration: 6
                    });
                }else{
                    $.ajax({
                        url: IP + 'commission',
                        type: 'PUT',
                        data: data,
                        // contentType:'application/json',
                        success: function (res) {
                            if (res.code == 200) {
                                that.$Message.info({
                                    content: '确认提成信息成功',
                                    duration: 6
                                });
                                $('#search-btn').click();
                            } else {
                                that.$Message.error({
                                    content: res.msg,
                                    duration: 6
                                });
                            }
                        },
                        error: function (res) {
                            console.log(res);
                            that.$Message.error({
                                content: '确认提成信息失败',
                                duration: 6
                            });
                        }
                    });
                }
                this.cancelPersentage();
            },
            cancelPersentage: function () {
                this.id = 0;
                this.contractSum = 0;
                this.salerName = '';
                this.percentage = 0;
                this.commission = 0;
                this.date = '';
                this.remark = '';
            }
        },
    });
    global.percentageModal = percentageModal;

    /**
     * 导出财务
     */
    var exportModal = new Vue({
        el: '#export-modal',
        data: {
            indeterminate: false,
            checkAll: false,
            show: false,
            checkList: ['0'],
            // contractList:new Set(),
            start: '',
            end: '',
            ex_start: '',
            ex_end: '',
            place: 0,
            contractType: 0,
            blockList: [],
            contractTypeList: [],
        },
        beforeMount() {
            var that = this;
            $.ajax({
                type: 'get',
                url: IP + "block",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.blockList = res.data;
                        that.blockList.unshift({
                            "id": 0,
                            "name": "全部",
                            "tag": ""
                        });
                    } else {
                        that.$Message.error({
                            content: '无法获取区块列表',
                            duration: 6
                        });
                    }
                }
            });
            $.ajax({
                type: 'get',
                url: IP + "contractType",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.contractTypeList = res.data;
                        that.contractTypeList.unshift({
                            "id": 0,
                            "name": "全部",
                            "tag": ""
                        });
                    } else {
                        that.$Message.error({
                            content: '无法获取合同类别列表',
                            duration: 6
                        });
                    }
                }
            });
        },
        methods: {
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                    return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
                } else {
                    return ''
                }
            },
            ok: function () {
                var sql = [];
                var map = {
                    '0': '',
                    '1': '1',
                    '2': '2',
                    '3': '3',
                    '4': '4',
                }
                this.checkList.forEach(e => {
                    sql += map[e];
                });
                var data = {
                    sql: sql,
                    block_id: parseInt(this.place),
                    contract_type: parseInt(this.contractType),
                    start_date: this.changeTS2ST(this.start),
                    end_date: this.changeTS2ST(this.end),
                    start_date_change: this.changeTS2ST(this.ex_start),
                    end_date_change: this.changeTS2ST(this.ex_end),
                }
                window.open(
                    IP + 'api/v1/actural/get_output?sql=' + data.sql + '&block_id=' + data.block_id + '&contract_type=' + data.contract_type + '&start_date=' + data.start_date + '&end_date=' + data.end_date + '&start_date_change=' + data.start_date_change + '&end_date_change=' + data.end_date_change, '_blank');
                this.cancel();
            },
            cancel: function () {
                this.checkList = ['0'];
                this.start = '';
                this.end = '';
                this.ex_start = '';
                this.ex_end = '';
                this.place = 0;
                this.contractType = 0;
                this.indeterminate = false;
                this.checkAll = false;
            },
            handleCheckAll() {
                if (this.indeterminate) {
                    this.checkAll = false;
                } else {
                    this.checkAll = !this.checkAll;
                }
                this.indeterminate = false;

                if (this.checkAll) {
                    this.checkList = ['0', '1', '2', '3', '4'];
                } else {
                    this.checkList = ['0'];
                }
            },
            checkAllGroupChange(data) {
                if (data.length === 5) {
                    this.indeterminate = false;
                    this.checkAll = true;
                } else if (data.length > 1) {
                    this.indeterminate = true;
                    this.checkAll = false;
                } else {
                    this.indeterminate = false;
                    this.checkAll = false;
                }
            }
        }
    });
    //向外暴露
    global.exportModal = exportModal;
})(window);