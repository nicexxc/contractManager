var that = this;
layui.use(['element', 'layer', 'table', 'form', 'laydate'], function () {
    var element = layui.element;
    var layer = layui.layer;
    var table = layui.table;
    var form = layui.form;
    var laydate = layui.laydate;
    var role = $.cookie('role');
    var judgeSP = authJudge(['saler','purchaser']);
    form.render();

    function unread(callback) {
        $.ajax({
            type: 'GET',
            url: IP + 'reminder/number',
            data: {},
            success: function (res) {
                if (res.code == 200) {
                    var count = res.data.count;
                    $.cookie('unread',count,{ path: '/' });
                    $('#unread-bubble').html(count);
                    if(typeof callback == "function"){
                        callback();
                    }
                }
            }
        });
    }
    table.render({
        elem: '#contractinfo',
        url: IP + "reminder",
        toolbar: '#toolbar-contractinfo1',
        id:'CONTRACT_INFO',
        //defaultToolbar: 'filter',
        where: {
            type: 1,
        },
        request: {
            pageName: 'pageNum', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": (res.code == 200 ? 0 : 1),
                "msg": "",
                "count": res.data.totalRecord,
                "data": res.data.arr
            };
        },
        title: '合同消息',
        totalRow: true,
        cols: [
            [{
                    title: '状态',
                    fixed: 'left',
                    templet: '#readStatus'
                },
                {
                    field: 'number',
                    title: '合同编号',
                    fixed: 'left',
                    width: "10%"
                },
                {
                    field: 'name',
                    title: '合同名称'
                },
                {
                    field: 'block',
                    title: '归属区块'
                },
                {
                    field: 'updateType',
                    title: '更新类别'
                },
                {
                    field: 'operater',
                    title: '操作人'
                },
                {
                    field: 'operaterTime',
                    title: '更新时间',
                    sort: true
                },
                {
                    fixed: 'right',
                    title: '操作',
                    toolbar: '#operationBar1',
                    unresize: true,
                    minWidth: 160
                },
            ]
        ],
        page: true,
    });
    table.on('toolbar(contractinfo)', function (obj) {
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        if (layEvent === 'setallread') { //全部已读
            layer.confirm('确认全部已读吗？', function (index) {
                //向服务端发送已读指令
                $.ajax({
                    type: 'GET',
                    url: IP + 'reminder/all',
                    data: {
                        type:1
                    },
                    success: function (res) {
                        if (res.code == 200) {
                            table.reload('CONTRACT_INFO');
                            //TODO:获取剩余消息数
                            unread();
                            layer.close(index);
                        } else {
                            layer.msg('无法确认已读', {
                                time: 2000, //2s后自动关闭
                            });
                            layer.close(index);
                        }
                    }
                });
            });
        }
    });
    table.on('tool(contractinfo)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        if (layEvent === 'contractdetail') { //查看
            $.ajax({
                type: 'GET',
                url: IP + 'reminder/' + data.id,
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        table.reload('CONTRACT_INFO');
                        //TODO:获取剩余消息数
                        unread(function () {
                            window.location.href = './readContract.html?id=' + data.contract_id + '&tag=' + (data.tag?1:0) + '&from=m';
                        });
                        layer.close(index);
                    } else {
                        layer.msg('无法确认已读', {
                            time: 2000, //2s后自动关闭
                        });
                        layer.close(index);
                    }
                }
            });
        } else if (layEvent === 'contractread') { //已读
            layer.confirm('确认已读吗？', function (index) {
                //向服务端发送已读指令
                $.ajax({
                    type: 'GET',
                    url: IP + 'reminder/read',
                    data: {id:data.id},
                    success: function (res) {
                        if (res.code == 200) {
                            table.reload('CONTRACT_INFO');
                            //TODO:获取剩余消息数
                            unread();
                            layer.close(index);
                        } else {
                            layer.msg('无法确认已读', {
                                time: 2000, //2s后自动关闭
                            });
                            layer.close(index);
                        }
                    }
                });
            });
        }
    });
    if(!judgeSP(role)){
        table.render({
            elem: '#financialinfo',
            url: IP + "reminder",
            toolbar: '#toolbar-contractinfo2',
            //defaultToolbar: 'filter',
            id:'FINANCIAL_INFO',
            where: {
                type: 2,
            },
            request: {
                pageName: 'pageNum', //页码的参数名称，默认：page
                limitName: 'record' //每页数据量的参数名，默认：limit
            },
            parseData: function (res) { //res 即为原始返回的数据
                return {
                    "code": (res.code == 200 ? 0 : 1),
                    "msg": res.msg,
                    "count": res.data.totalRecord,
                    "data": res.data.arr
                };
            },
            title: '财务消息',
            totalRow: true,
            cols: [
                [
                    {
                        title: '状态',
                        fixed: 'left',
                        templet: '#readStatus'
                    },
                    {
                        field: 'number',
                        title: '合同编号',
                        fixed: 'left',
                        width: "10%"
                    },
                    {
                        field: 'name',
                        title: '合同名称'
                    },
                    {
                        field: 'block',
                        title: '归属区块'
                    },
                    {
                        field: 'updateType',
                        title: '更新类别'
                    },
                    {
                        field: 'operater',
                        title: '操作人'
                    },
                    {
                        field: 'operaterTime',
                        title: '更新时间',
                        sort: true
                    },
                    {
                        fixed: 'right',
                        title: '操作',
                        toolbar: operationBar2,
                        unresize: true,
                        minWidth: 160
                    },
                ]
            ],
            page: true,
        });
        table.on('toolbar(financialinfo)', function (obj) {
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
    
            if (layEvent === 'setallread') { //全部已读
                layer.confirm('确认全部已读吗？', function (index) {
                    //向服务端发送已读指令
                    $.ajax({
                        type: 'GET',
                        url: IP + 'reminder/all',
                        data: {
                            type:2
                        },
                        success: function (res) {
                            if (res.code == 200) {
                                table.reload('FINANCIAL_INFO');
                                unread();
                                layer.close(index);
                            } else {
                                layer.msg('无法确认已读', {
                                    time: 2000, //2s后自动关闭
                                });
                                layer.close(index);
                            }
                        }
                    });
                });
            }
        });
        table.on('tool(financialinfo)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
    
            if (layEvent === 'financialdetail') { //查看
                $.ajax({
                    type: 'GET',
                    url: IP + 'reminder/' + data.id,
                    data: {},
                    success: function (res) {
                        if (res.code == 200) {
                            table.reload('FINANCIAL_INFO');
                            unread(function () {
                                window.location.href = './financedetailinfo.html?id='+data.contract_id+'&tag='+data.tag+'&from=m';
                            });
                            layer.close(index);
                        } else {
                            layer.msg('无法确认已读', {
                                time: 2000, //2s后自动关闭
                            });
                            layer.close(index);
                        }
                    }
                });
            } else if (layEvent === 'financialread') { //已读
                layer.confirm('确认已读吗？', function (index) {
                    //向服务端发送已读指令
                    $.ajax({
                        type: 'GET',
                        url: IP + 'reminder/read',
                        data: {id:data.id},
                        success: function (res) {
                            if (res.code == 200) {
                                table.reload('FINANCIAL_INFO');
                                unread();
                                layer.close(index);
                            } else {
                                layer.msg('无法确认已读', {
                                    time: 2000, //2s后自动关闭
                                });
                                layer.close(index);
                            }
                        }
                    });
                });
            }
        });
    }
});