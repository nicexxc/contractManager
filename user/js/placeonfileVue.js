(function (global) {
    var body = new Vue({
        el: '#contract-body',
        data: {
            contractId: 0, //合同ID
            cabinetNum: '', //档案柜号
            archiveDate: '', //归档日期
            writeMan: '', //数据录入员
            curator: '', //保管员
            remark: '', //备注
            contractFiles: [],
            enclosureFiles: []
        },
        beforeMount() {
            var name = $.cookie('nickname');
            var id = window.location.href.split('?')[1].split('=')[1];
            this.contractId = parseInt(id, 10);
            this.writeMan = name;
        },
        methods: {
            /**
             * 时间转换
             * @param {timestamp} ts js默认时间格式
             */
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                    return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
                }
            },
            deleteContractFile: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: '是否删除这个文件？',
                    onOk: function () {
                        that.contractFiles.splice(index, 1);
                    }
                });
            },
            addToContractContainer: function (name) {
                this.contractFiles.push(name);
            },
            deleteEnclosureFile: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: '是否删除这个文件？',
                    onOk: function () {
                        that.enclosureFiles.splice(index, 1);
                    }
                });
            },
            addToEnclosureContainer: function (name) {
                this.enclosureFiles.push(name);
            },
            fileIt: function () {
                var that = this;
                this.$Modal.confirm({
                    title: '确定归档该合同吗？',
                    onOk: function () {
                        if (that.cabinetNum == '') {
                            that.$Message.error({
                                content: '请填写档案柜号',
                                duration: 6
                            });
                        } else if (that.curator == '') {
                            that.$Message.error({
                                content: '请填写保管员',
                                duration: 6
                            });
                        } else {
                            var formData = new FormData();
                            formData.append('contractId', that.contractId);
                            formData.append('clerk', that.curator);
                            formData.append('positionNumber', that.cabinetNum);
                            formData.append('comment', that.remark);
                            formData.append('archiveDate', that.changeTS2ST(that.archiveDate));
                            formData.append('contractContents', that.contractFiles.join(','));
                            formData.append('others', that.enclosureFiles.join(','));
                            global.$.ajax({
                                url: IP + 'archive_contract',
                                type: 'POST',
                                data: formData,
                                processData: false,
                                contentType: false,
                                mimeType: 'multipart/form-data',
                                success: function (res) {
                                    var res = eval("(" + res + ")")
                                    if (res.code == 200) {
                                        global.location.href = IP_ + 'user/htmlPages/fileContractList.html';
                                    } else {
                                        that.$Message.error({
                                            content: res.msg,
                                            duration: 6
                                        });
                                    }
                                },
                                error: function (res) {
                                    console.log(res);
                                    that.$Message.error({
                                        content: '归档失败',
                                        duration: 6
                                    });
                                }
                            });
                        }
                    }
                });
            },
            cancel: function () {
                this.$Modal.confirm({
                    title: '确定取消归档吗？',
                    onOk: function () {
                        global.location.href = IP_ + 'user/htmlPages/fileContractList.html';
                    }
                });
            }
        },
    });
    global.body = body;
})(window)