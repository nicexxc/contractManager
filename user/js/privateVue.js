(function (global) {
    var body = new Vue({
        el: "#body-center",
        data: {
            originPwd: '',
            newPwd: '',
            confirmNewPwd: ''
        },
        methods: {
            confirm: function () {
                var that = this;
                if (this.originPwd == '') {
                    this.$Message.error({
                        content: '请填写原密码',
                        duration: 6
                    });
                } else if (this.newPwd == '') {
                    this.$Message.error({
                        content: '请填写新密码',
                        duration: 6
                    });
                } else if (this.confirmNewPwd == '') {
                    this.$Message.error({
                        content: '请填写确认新密码',
                        duration: 6
                    });
                } else if (this.confirmNewPwd != this.newPwd) {
                    this.$Message.error({
                        content: '两次密码输入不同',
                        duration: 6
                    });
                } else {
                    $.ajax({
                        url: IP + 'password',
                        type: 'POST',
                        data: {
                            originPassword: that.originPwd,
                            password: that.newPwd
                        },
                        success: function (res) {
                            if (res.code == 200) {
                                that.$Modal.confirm({
                                    title: '修改成功',
                                    onOk: function () {
                                        $.ajax({
                                            type:'get',
                                            url:IP+'logout',
                                            data:{},
                                            success:function (res) {
                                                if(res.code == 200){
                                                    window.location.href = IP_ + 'login.html'
                                                }
                                            }
                                        });
                                    }
                                });
                            } else if (res.code == 501) {
                                that.$Message.error({
                                    content: '原密码错误',
                                    duration: 6
                                });
                            } else {
                                that.$Message.error({
                                    content: '修改失败',
                                    duration: 6
                                });
                            }
                        },
                        error: function (res) {
                            console.log(res);
                            that.$Message.error({
                                content: '请求失败',
                                duration: 6
                            });
                        }
                    });
                }
            }
        },
    });
})(window);