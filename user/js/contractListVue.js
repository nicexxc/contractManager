(function (global) {
    /**
     * 归属区块
     */
    var contractPlace = new Vue({
        el: "#contract-place",
        data: {
            list: [
                /*{
                    "id": 0,
                    "name": "",
                    "tag": ""
                }*/
            ],
            place: 0,
            showBlock: true
        },
        beforeMount: function () {
            var that = this;
            var role = $.cookie('role');
            var judgeArea = authJudge(['areaCheif', 'operator']);
            if (judgeArea(role)) {
                this.showBlock = false
            }
            $.ajax({
                type: 'get',
                url: IP + "block",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.list = res.data;
                        that.list.unshift({
                            "id": 0,
                            "name": "全部",
                            "tag": ""
                        });
                    } else {
                        that.$Message.error({
                            content: '无法获取区块列表',
                            duration: 6
                        });
                    }
                }
            });
        },
        methods: {
            flash: function () {
                $('#search-btn').click();
            }
        }
    });
    //向外暴露
    global.contractPlace = contractPlace;

    /**
     * 合同类别
     * 
     */
    var contractType = new Vue({
        el: "#contract-type",
        data: {
            contractTypeList: [
                /*{
                            "id": 0,
                            "name": "",
                            "tag": ""
                        }*/
            ],
            contractType: 0,
        },
        beforeMount: function () {
            var that = this;
            $.ajax({
                type: 'get',
                url: IP + "contractType",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.contractTypeList = res.data;
                        that.contractTypeList.unshift({
                            "id": 0,
                            "name": "全部",
                            "tag": ""
                        });
                    } else {
                        that.$Message.error({
                            content: '无法获取合同类别列表',
                            duration: 6
                        });
                    }
                }
            });
        },
        methods: {
            flash: function () {
                $('#search-btn').click();
            }
        }
    });
    //向外暴露
    global.contractType = contractType;

    var searchDate = new Vue({
        el: '#search-date',
        data: {
            start: '',
            end: ''
        },
        methods: {
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                    return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
                }
            },
            flash: function () {
                $('#search-btn').click();
            }
        }
    });
    //向外暴露
    global.searchDate = searchDate;
    /**
     * 归档状态
     */
    var fileState = new Vue({
        el: '#search-state',
        data: {
            state: 0
        },
        methods: {
            flash: function () {
                $('#search-btn').click();
            }
        }
    });
    //向外暴露
    global.fileState = fileState;

    /**
     * 追加节点
     */
    var contractAddNote = new Vue({
        el: "#add-node",
        data: {
            list: [],
            disabled: ['disabled', '', '', ''],
            date: '',
            node: '',
            nodeOther: '',
            id: ''
        },
        methods: {
            choiseNode: function () {
                var that = this;
                this.$Modal.confirm({
                    render(h) {
                        return h('div', {}, [
                            h('span', {}, '追加日期：'),
                            h('DatePicker', {
                                props: {
                                    type: 'date',
                                    placeholder: '请选择日期'
                                },
                                on: {
                                    'on-change': function (val) {
                                        that.date = val;
                                    }
                                }
                            }),
                            h('RadioGroup', {
                                props: {
                                    vertical: true
                                },
                                on: {
                                    'on-change': function (val) {
                                        that.node = val;
                                    }
                                }
                            }, [
                                h('Radio', {
                                    props: {
                                        label: '备货'
                                    }
                                }),
                                h('Radio', {
                                    props: {
                                        label: '发货'
                                    }
                                }),
                                h('Radio', {
                                    props: {
                                        label: '安装'
                                    }
                                }),
                                h('Radio', {
                                    props: {
                                        label: '验收'
                                    }
                                }),
                                h('Radio', {
                                    props: {
                                        label: '其他'
                                    }
                                }, [
                                    h('Input', {
                                        props: {
                                            placeholder: '请输入其他节点'
                                        },
                                        on: {
                                            'on-change': function (e) {
                                                that.nodeOther = e.target.value;
                                            }
                                        }
                                    })
                                ]),
                            ])
                        ])
                    },
                    onOk: function () {
                        var date = that.date;
                        var node = that.node;
                        if (node == '其他') {
                            node = that.nodeOther;
                        }
                        if (!node) {
                            this.$Message.error({
                                content: '请选择节点',
                                duration: 6
                            });
                        } else if (!date) {
                            this.$Message.error({
                                content: '请选择日期',
                                duration: 6
                            });
                        } else {
                            $.ajax({
                                type: 'post',
                                url: IP + "executeNode",
                                contentType: 'application/json',
                                data: JSON.stringify({
                                    contractId: that.id,
                                    executeDate: date,
                                    nodeName: node
                                }),
                                success: function (res) {
                                    if (res.code == 200) {
                                        that.list.push({
                                            nodeName: node,
                                            executeDate: date
                                        });
                                    } else {
                                        that.$Message.error({
                                            content: '无法追加节点',
                                            duration: 6
                                        });
                                    }
                                }
                            });
                            that.date = null;
                            that.node = null;
                            that.nodeOther = null;
                        }
                    },
                    onCancel: function () {
                        that.date = null;
                        that.node = null;
                        that.nodeOther = null;
                    }
                });
            },
            close: function () {
                $("#add-node").fadeOut();
                $(".mash").fadeOut();
            },
            getList: function () {
                var that = this;
                $.ajax({
                    type: 'get',
                    url: IP + "executeNode/" + that.id,
                    data: {},
                    success: function (res) {
                        if (res.code == 200) {
                            that.list = res.data;
                        } else {
                            that.$Message.error({
                                content: '无法获取执行节点列表',
                                duration: 6
                            });
                        }
                    }
                });
            }
        }
    });
    //向外暴露
    global.contractAddNote = contractAddNote;

    /**
     * 导出合同
     */
    var exportModal = new Vue({
        el: '#export-modal',
        data: {
            indeterminate: false,
            checkAll: false,
            show: false,
            checkList: ['0'],
            // contractList:new Set(),
            start: '',
            end: '',
            place: 0,
            contractType: 0,
            blockList: [],
            contractTypeList: [],
        },
        beforeMount() {
            var that = this;
            $.ajax({
                type: 'get',
                url: IP + "block",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.blockList = res.data;
                        that.blockList.unshift({
                            "id": 0,
                            "name": "全部",
                            "tag": ""
                        });
                    } else {
                        that.$Message.error({
                            content: '无法获取区块列表',
                            duration: 6
                        });
                    }
                }
            });
            $.ajax({
                type: 'get',
                url: IP + "contractType",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.contractTypeList = res.data;
                        that.contractTypeList.unshift({
                            "id": 0,
                            "name": "全部",
                            "tag": ""
                        });
                    } else {
                        that.$Message.error({
                            content: '无法获取合同类别列表',
                            duration: 6
                        });
                    }
                }
            });
        },
        methods: {
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                    return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
                } else {
                    return ''
                }
            },
            ok: function () {
                var sql = [];
                var map = {
                    '0': '合同名称,合同类别,合同内容类别,归属区块,合同总额',
                    '1': '签订日期',
                    '2': '经办人',
                    '3': '销售业务员',
                    '4': '技术服务员',
                    '5': '客户',
                }
                this.checkList.forEach(e => {
                    sql.push(map[e]);
                });
                var data = {
                    sql: sql.join(','),
                    block_id: parseInt(this.place),
                    contract_type: parseInt(this.contractType),
                    start_date: this.changeTS2ST(this.start),
                    end_date: this.changeTS2ST(this.end),
                }
                window.open(IP + 'get_output?sql=' + data.sql + '&block_id=' + data.block_id + '&contract_type=' + data.contract_type + '&start_date=' + data.start_date + '&end_date=' + data.end_date, '_blank')
                this.cancel();
            },
            cancel: function () {
                this.checkList = ['0'];
                this.start = '';
                this.end = '';
                this.place = 0;
                this.contractType = 0;
                this.checkAll = false;
                this.indeterminate = false;
            },
            handleCheckAll() {
                if (this.indeterminate) {
                    this.checkAll = false;
                } else {
                    this.checkAll = !this.checkAll;
                }
                this.indeterminate = false;

                if (this.checkAll) {
                    this.checkList = ['0', '1', '2', '3', '4', '5'];
                } else {
                    this.checkList = ['0'];
                }
            },
            checkAllGroupChange(data) {
                if (data.length === 6) {
                    this.indeterminate = false;
                    this.checkAll = true;
                } else if (data.length > 1) {
                    this.indeterminate = true;
                    this.checkAll = false;
                } else {
                    this.indeterminate = false;
                    this.checkAll = false;
                }
            }
        }
    });
    //向外暴露
    global.exportModal = exportModal;
})(window);