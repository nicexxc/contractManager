(function (global) {
    function getParams() {
        var url = document.location.toString();
        var params = url.split('?')[1].split('&');
        var id = params[0].split('=')[1];
        var tag = params[1].split('=')[1];
        return {
            id: id,
            tag: tag
        }
    }

    var body = new Vue({
        el: '#body-center',
        data: {
            //合同类别,合同内容类别
            contractTypeList: [
                /*{
                                "id": 0,
                                "name": "",
                                "tag": ""
                            }*/
            ],
            contentTypeList: [
                /*{
                                "id": 0,
                                "contractType": 0,
                                "name": ""
                            }*/
            ],
            contractType: 0,
            contractContentType: 0,

            //归属区块
            placeList: [
                /*{
                                "id": 0,
                                "name": "",
                                "tag": ""
                            }*/
            ],
            block: 0,

            //经办人
            operatorList: [],
            operatorName: '',

            //销售业务员
            showSalerList: [],
            salerValue: '',
            salerList: [
                /*{
                                "name": "",
                                "id": 0
                            }*/
            ],

            //技术服务员
            techList: [],

            //是否为运维合同
            show: true, //false为运维合同,

            //其他合同信息
            id: 0,
            number: '',
            name: '',
            create_date: '',
            customer: '',
            due: '',
            amount: 0,
            content: '',
            earnest: 0, //定金
            received: 0, //预收款
            progress: 0, //进度款
            delivery: 0, //发货款
            acceptance: 0, //验收款
            guarantee: 0, //质保金
            sign_date: '',
            service_start: '',
            service_end: ''
        },
        beforeMount: function () {
            var that = this;
            var params = getParams();
            var tag = params.tag;
            if (tag == 1) {
                this.show = false;
            } else {
                this.show = true;
            }
            var contractP = new Promise(function (resolve, reject) {
                //获取合同类别列表
                $.ajax({
                    type: 'get',
                    url: IP + "contractType",
                    data: {},
                    success: function (res) {
                        if (res.code == 200) {
                            that.contractTypeList = res.data;
                            resolve();
                        } else {
                            that.$Message.error({
                                content: '无法获取合同类别列表',
                                duration: 6
                            });
                            reject();
                        }
                    }
                });
            });
            var placeP = new Promise(function (resolve, reject) {
                //获取区块列表
                $.ajax({
                    type: 'get',
                    url: IP + "block",
                    data: {},
                    success: function (res) {
                        if (res.code == 200) {
                            that.placeList = res.data;
                            resolve();
                        } else {
                            that.$Message.error({
                                content: '无法获取区块列表',
                                duration: 6
                            });
                            reject();
                        }
                    }
                });
            });
            var salerP = new Promise(function (resolve, reject) {
                //获取销售业务员列表
                $.ajax({
                    type: 'get',
                    url: IP + "staff/saler",
                    data: {},
                    success: function (res) {
                        if (res.code == 200) {
                            that.salerList = res.data;
                            resolve();
                        } else {
                            that.$Message.error({
                                content: '无法获取销售员列表',
                                duration: 6
                            });
                            reject();
                        }
                    }
                });
            });
            Promise.all([contractP, placeP, salerP]).then(function () {
                return new Promise(function (resolve, reject) {
                    //获取合同信息
                    $.ajax({
                        type: 'GET',
                        url: IP + "contract",
                        data: {
                            id: params.id
                        },
                        success: function (res) {
                            if (res.code == 200) {
                                var d = res.data;
                                d.operator = d.operator == '' ? [] : d.operator.split(',');
                                d.technician = d.technician == '' ? [] : d.technician.split(',');
                                var nodeMoneys = d.nodeMoneys;

                                that.id = d.id;
                                if (nodeMoneys) {
                                    that.earnest = nodeMoneys[0] || 0; //定金
                                    that.received = nodeMoneys[1] || 0; //预收款
                                    that.progress = nodeMoneys[2] || 0; //进度款
                                    that.delivery = nodeMoneys[3] || 0; //发货款
                                    that.acceptance = nodeMoneys[4] || 0; //验收款
                                    that.guarantee = nodeMoneys[5] || 0; //质保金
                                }
                                that.number = d.number || '';
                                that.name = d.name || '';
                                that.block = that.getAnotheValueByValue(that.placeList, 'name', 'id', d.block);
                                that.showSalerList = d.sellers || [];
                                d.sellers.forEach(s => {
                                    that.salerList.forEach((e, i) => {
                                        if (e.id == s.sellerId) {
                                            that.salerList.splice(i, 1);
                                        }
                                    })
                                });
                                that.operatorList = d.operator || [];
                                that.techList = d.technician || [];
                                that.customer = d.customer || '';
                                that.due = d.due || '';
                                that.amount = d.amount || 0;
                                that.content = d.content || '';
                                that.create_date = d.create_date || '';
                                that.contractType = that.getAnotheValueByValue(that.contractTypeList, 'name', 'id', d.contract_type);
                                that.sign_date = d.sign_date || '';
                                that.service_start = d.service_start || '';
                                that.service_end = d.service_end || '';
                                resolve(d);
                            } else {
                                that.$Message.error({
                                    content: '无法获取合同信息',
                                    duration: 6
                                });
                                reject();
                            }
                        }
                    });
                });
            }).then(function (d) {
                //获取合同内容类别列表
                $.ajax({
                    type: 'get',
                    url: IP + "contentType",
                    data: {
                        contractTypeId: that.contractType
                    },
                    success: function (res) {
                        if (res.code == 200) {
                            that.contentTypeList = res.data;
                            that.contractContentType = that.getAnotheValueByValue(that.contentTypeList, 'name', 'id', d.content_type);
                        } else {
                            that.$Message.error({
                                content: '无法获取合同内容类别列表',
                                duration: 6
                            });
                        }
                    }
                });
            });
        },
        methods: {
            getAnotheValueByValue: function (list, valueKey, wantKey, value) {
                var len = list.length;
                for (var i = 0; i < len; i++) {
                    if (list[i][valueKey] == value) {
                        return list[i][wantKey];
                    }
                }
                return 0;
            },
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                } else {
                    date = new Date();
                }
                return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
            },
            //选择合同类别后，重新获取合同内容类别列表
            changeContentType: function () {
                this.contentTypeList = [];
                this.contractContentType = '';
                var that = this;
                $.ajax({
                    type: 'get',
                    url: IP + "contentType",
                    data: {
                        contractTypeId: that.contractType
                    },
                    success: function (res) {
                        if (res.code == 200) {
                            that.contentTypeList = res.data;
                        } else {
                            that.$Message.error({
                                content: '无法获取合同内容类别列表',
                                duration: 6
                            });
                        }
                    }
                });
            },
            deleteOperator: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: "提示",
                    content: "确定要删除此人吗？",
                    onOk: function () {
                        that.operatorList.splice(index, 1);
                    }
                });
            },
            addOperator: function () {
                var that = this;
                this.$Modal.confirm({
                    content: '<input class="layui-input" id="operator-name" placeholder="请输入名字"></input>',
                    onOk: function () {
                        var name = $('#operator-name').val();
                        if (name == null || name == '') {
                            this.$Message.error({
                                content: '请输入姓名',
                                duration: 6
                            });
                        } else {
                            that.operatorList.push(name)
                        }
                    }
                });
            },
            deleteSaler: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: "提示",
                    content: "确定要删除此人吗？",
                    onOk: function () {
                        var saler = that.showSalerList.splice(index, 1)[0];
                        console.log(saler);
                        that.salerList.push({
                            name: saler.name,
                            id: saler.sellerId
                        });
                    }
                });
            },
            addSaler: function () {
                var that = this;
                this.$Modal.confirm({
                    render(h) {
                        var salerList = that.salerList;
                        var optionList = [];
                        for (var i = 0; i < salerList.length; i++) {
                            var person = salerList[i];
                            var id = person.id;
                            var name = person.name;
                            optionList.push(h('Option', {
                                props: {
                                    value: id + "#" + name,
                                    key: id
                                }
                            }, name))
                        }
                        return h('Select', {
                            props: {
                                value: that.id
                            },
                            on: {
                                'on-change': function (val) {
                                    that.salerValue = val
                                }
                            }
                        }, optionList)
                    },
                    onOk() {
                        if (that.salerValue) {
                            var v = that.salerValue.split('#');
                            that.showSalerList.push({
                                sellerId: parseInt(v[0], 10),
                                name: v[1]
                            });
                            that.salerList.forEach((e, i) => {
                                if (e.id == parseInt(v[0], 10)) {
                                    that.salerList.splice(i, 1);
                                }
                            });
                            that.salerValue = '';
                        }
                    }
                });
            },
            deleteTech: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: "提示",
                    content: "确定要删除此人吗？",
                    onOk: function () {
                        that.techList.splice(index, 1);
                    }
                });
            },
            addTech: function () {
                var that = this;
                this.$Modal.confirm({
                    content: '<input class="layui-input" id="operator-name" placeholder="请输入名字"></input>',
                    onOk: function () {
                        var name = $('#operator-name').val();
                        if (name == null || name == '') {
                            this.$Message.error({
                                content: '请输入姓名',
                                duration: 6
                            });
                        } else {
                            that.techList.push(name)
                        }
                    }
                });
            },
            cancel: function () {
                global.history.go(-1);
            },
            save: function () {
                var that = this;
                var type = (getParams().tag == 1);

                var data = {
                    id: that.id,
                    amount: that.amount, //合同总金额
                    block: that.block, //区块id
                    content: that.content, //合同简介
                    contractType: that.contractType, //合同类别id
                    contentType: that.contractContentType, //合同内容类别id
                    createDate: that.create_date, //创建日期
                    customer: that.customer, //客户
                    signDate: that.changeTS2ST(that.sign_date), //合同签订日期
                    due: that.changeTS2ST(that.due), //合同到期日期
                    name: that.name, //合同名称
                    operator: that.operatorList.join(','), //经办人
                    technician: that.techList.join(','), //技术服务员
                    operationTag: type, //是否为运维合同
                    contractSellerList: that.showSalerList, //销售员id
                }
                //根据不同合同类别添加字段
                if (!type) {
                    //合同节点list
                    data.contractNodeList = [{
                            "money": parseFloat(that.earnest) || 0,
                            "name": "定金"
                        },
                        {
                            "money": parseFloat(that.received) || 0,
                            "name": "预收款"
                        },
                        {
                            "money": parseFloat(that.progress) || 0,
                            "name": "进度款"
                        },
                        {
                            "money": parseFloat(that.delivery) || 0,
                            "name": "发货款"
                        },
                        {
                            "money": parseFloat(that.acceptance) || 0,
                            "name": "验收款"
                        },
                        {
                            "money": parseFloat(that.guarantee) || 0,
                            "name": "质保金"
                        },
                    ];
                } else {
                    //运维合同服务开始时间
                    data.serviceStart = that.changeTS2ST(that.service_start);
                    //运维合同服务结束时间
                    data.serviceEnd = that.changeTS2ST(that.service_end);
                }

                //TODO:校验
                if (that.contractType == 0) {
                    that.$Message.error({
                        content: '请选择合同类别',
                        duration: 6
                    });
                } else if (that.contractContentType == 0) {
                    that.$Message.error({
                        content: '请选择合同内容类别',
                        duration: 6
                    });
                } else if (that.name == '') {
                    that.$Message.error({
                        content: '请输入合同名称',
                        duration: 6
                    });
                } else if (that.block == 0) {
                    that.$Message.error({
                        content: '请选择区块',
                        duration: 6
                    });
                } else if (that.operatorList.length == 0) {
                    that.$Message.error({
                        content: '请填写经办人',
                        duration: 6
                    });
                } else if (that.showSalerList.length == 0) {
                    that.$Message.error({
                        content: '请填写销售业务员',
                        duration: 6
                    });
                } else if (that.techList.length == 0) {
                    that.$Message.error({
                        content: '请填写技术服务员',
                        duration: 6
                    });
                } else if (that.customer == '') {
                    that.$Message.error({
                        content: '请输入客户名称',
                        duration: 6
                    });
                } else if (that.sign_date == '') {
                    that.$Message.error({
                        content: '请选择合同签订日期',
                        duration: 6
                    });
                } else if (!that.show && that.due == '') {
                    that.$Message.error({
                        content: '请选择合同到期日期',
                        duration: 6
                    });
                } else if (that.show && (that.amount < (that.earnest + that.received + that.progress + that.delivery + that.acceptance + that.guarantee))) {
                    that.$Message.error({
                        content: '请检查合同节点金额，总合超出合同额',
                        duration: 6
                    });
                } else {
                    //请求
                    $.ajax({
                        type: 'PUT',
                        url: IP + "contract",
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function (res) {
                            if (res.code == 200) {
                                that.$Message.info({
                                    content: '修改合同成功',
                                    duration: 6
                                });
                                window.history.go(-1);
                            } else {
                                that.$Message.error({
                                    content: '无法修改合同',
                                    duration: 6
                                });
                            }
                        },
                        error: function () {
                            that.$Message.error({
                                content: '无法修改合同',
                                duration: 6
                            });
                        }
                    });
                }
            }
        }
    });
    global.body = body;
})(window);