layui.use(['element','layer','upload'], function(){
    var element = layui.element;
    var layer = layui.layer;
    var upload = layui.upload;
    var xhrOnProgress = function (fun) {
        xhrOnProgress.onprogress = fun; //绑定监听
        //使用闭包实现监听绑
        return function () {
            //通过$.ajaxSettings.xhr();获得XMLHttpRequest对象
            var xhr = $.ajaxSettings.xhr();
            //判断监听函数是否为函数
            if (typeof xhrOnProgress.onprogress !== 'function')
                return xhr;
            //如果有监听函数并且xhr对象支持绑定时就把监听函数绑定上去
            if (xhrOnProgress.onprogress && xhr.upload) {
                xhr.upload.onprogress = xhrOnProgress.onprogress;
            }
            return xhr;
        }
    }
    upload.render({
        accept: "file",
        elem: "#addContract",
        url: IP + "upload",
        xhr: xhrOnProgress,
        progress: function (value) { //上传进度回调 value进度值
            element.progress('progress-contract', value + '%') //设置页面进度条
        },
        done: function (res, index, upload) {
            if (res.code == 200) {
                body.addToContractContainer(res.data.filename); //body位于editFilePageVue.js
            }
        },
        error: function (res) {
            console.log(res)
        }
    });
    upload.render({
        accept: "file",
        elem: "#addEnclosure",
        url: IP + "upload",
        xhr: xhrOnProgress,
        progress: function (value) { //上传进度回调 value进度值
            element.progress('progress-enclosure', value + '%') //设置页面进度条
        },
        done: function (res, index, upload) {
            if (res.code == 200) {
                body.addToEnclosureContainer(res.data.filename);//body位于editFilePageVue.js
            }
        },
        error: function (res) {
            console.log(res)
        }
    });
});