(function (global) {
    var role = $.cookie('role');
    var bread = new Vue({
        el:'#bread',
        data:{
            tag:true
        },
        beforeMount(){
            var params = getParams();
            var from = params.from;
            if(from == 'm'){
                this.tag = false;
            }
        }
    });
    global.bread = bread;
    var body = new Vue({
        el: '#contract-body',
        data: {
            nodeList: [],
            show: true,
            content: '',
            service_end: '',
            service_start: '',
            nodeMoneys: [],
            earnest: 0,
            received: 0,
            progress: 0,
            delivery: 0,
            acceptance: 0,
            guarantee: 0,
            amount: 0,
            due: '',
            sign_date: '',
            customer: '',
            technician: [],
            sellers: [],
            operator: [],
            block: '',
            name: '',
            content_type: '',
            contract_type: '',
            create_date: '',
            number: '',
            purchase:false,
        },
        beforeMount() {
            var that = this;
            var params = getParams();
            var tag = params.tag;
            var from = params.from;
            var judgeSaler = authJudge(['saler']);
            if (tag == 1) {
                this.show = false;
            } else {
                this.show = true;
            }
            if(judgeSaler(role)){
                this.purchase = true;
            }
            $.ajax({
                type: 'GET',
                url: IP + "contract",
                data: {
                    id: params.id
                },
                success: function (res) {
                    if (res.code == 200) {
                        var d = res.data;
                        that.operator = d.operator ? d.operator.split(',') : [];
                        that.technician = d.technician ? d.technician.split(',') : [];
                        that.content = d.content;
                        that.service_end = d.service_end;
                        that.service_start = d.service_start;
                        if (d.nodeMoneys) {
                            that.earnest = d.nodeMoneys[0];
                            that.received = d.nodeMoneys[1];
                            that.progress = d.nodeMoneys[2];
                            that.delivery = d.nodeMoneys[3];
                            that.acceptance = d.nodeMoneys[4];
                            that.guarantee = d.nodeMoneys[5];
                        }
                        that.amount = d.amount;
                        that.due = d.due;
                        that.sign_date = d.sign_date;
                        that.customer = d.customer;
                        that.sellers = d.sellers;
                        that.block = d.block;
                        that.name = d.name;
                        that.content_type = d.content_type;
                        that.contract_type = d.contract_type;
                        that.create_date = d.create_date;
                        that.number = d.number;
                    } else {
                        that.$Message.error({
                            content: '无法获取合同信息',
                            duration: 6
                        });
                    }
                },
                error:function () {
                    that.$Message.error({
                        content: '无法获取合同信息',
                        duration: 6
                    });
                }
            });
            $.ajax({
                type: 'get',
                url: IP + "executeNode/" + params.id,
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.nodeList = res.data;
                    } else {
                        that.$Message.error({
                            content: '无法获取执行节点列表',
                            duration: 6
                        });
                    }
                },
                error:function(){
                    that.$Message.error({
                        content: '无法获取执行节点列表',
                        duration: 6
                    });
                }
            });
        }
    })
    global.body = body;

    function getParams() {
        var url = document.location.toString();
        var params = url.split('?')[1].split('&');
        var id = params[0].split('=')[1];
        var tag = params[1].split('=')[1];
        var from = (params[2] || '').split('=')[1];
        return {
            id: id,
            tag: tag,
            from: from
        }
    }
})(window)