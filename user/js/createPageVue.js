(function (global) {
    var createBody = new Vue({
        el: '#create-body',
        data: {
            contract_id: "自动生成",
            contract_date: '',
            show: 2, //true为显示合同节点金额，false为显示合同节点时间
            /**
             * 合同类别,合同内容类别
             */
            contractTypeList: [
                /*{
                    "id": 0,
                    "name": "",
                    "tag": ""
                }*/
            ],
            contentTypeList: [
                /*{
                    "id": 0,
                    "contractType": 0,
                    "name": ""
                }*/
            ],
            contractType: 0,
            contractContentType: 0,
            /**
             * 归属区块
             */
            placeList: [
                /*{
                    "id": 0,
                    "name": "",
                    "tag": ""
                }*/
            ],
            block: 0,
            blockWritable:false,
            /**
             * 经办人
             */
            operatorList: [],
            /**
             * 销售业务员
             */
            salerShowList: [],
            value: '',
            salerList: [
                /*{
                    "name": "",
                    "id": 0
                }*/
            ],
            /**
             * 技术服务员
             */
            techList: [],

            /**
             * 其他
             */
            name: '', //合同名
            customer: '', //客户名称
            sign_date: '', //合同签订日期
            due: '', //合同到期日期
            amount: 0, //合同金额
            earnest: 0, //定金
            received: 0, //预收款
            progress: 0, //进度款
            delivery: 0, //发货款
            acceptance: 0, //验收款
            guarantee: 0, //质保金
            service_start: '', //合同节点时间
            service_end: '', //合同节点时间
            content: '', //合同简介
            optionsToday:{
                disabledDate (date){
                    return date.valueOf() > Date.now();
                }
            },
            optionsEnd:{
                disabledDate:(date) => {
                    return (date.valueOf() < new Date(this.service_start).valueOf()) || (date.valueOf() < Date.now());
                }
            }
        },
        beforeMount: function () {
            var that = this;
            var role = $.cookie('role');
            var judgeArea = authJudge(['areaCheif','operator']);
            that.contract_date = that.changeTS2ST();
            if(judgeArea(role)){
                that.blockWritable = true;
            }
            $.ajax({
                type: 'get',
                url: IP + "contractType",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.contractTypeList = res.data;
                    } else {
                        that.$Message.error({
                            content: '无法获取合同类别列表',
                            duration: 6
                        });
                }
                    }
            });
            $.ajax({
                type: 'get',
                url: IP + "block",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.placeList = res.data;
                        if(judgeArea(role)){
                            $.ajax({
                                url:IP + 'staff/block',
                                type:'GET',
                                data:{},
                                success:function (res) {
                                    if(res.data.blockId == 0){
                                        that.block = 0;
                                        that.$Message.error({
                                            content: '未分配区块',
                                            duration: 6
                                        });
                                    }else{
                                        that.block = res.data.blockId;
                                    }
                                }
                            });
                        }
                    } else {
                        that.$Message.error({
                            content: '无法获取区块列表',
                            duration: 6
                        });
                    }
                }
            });
            $.ajax({
                type: 'get',
                url: IP + "staff/saler",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.salerList = res.data;
                    } else {
                        that.$Message.error({
                            content: '无法获取销售员列表',
                            duration: 6
                        });
                    }
                }
            });
        },
        methods: {
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                } else {
                    date = new Date();
                }
                return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
            },
            changeContentType: function (e) {
                this.contentTypeList = [];
                this.contractContentType = '';
                var label = e.label;
                if(label == '运维技术服务' || label == '技术咨询'){
                    this.show = 1;
                }else{
                    this.show = 0;
                }
                var that = this;
                $.ajax({
                    type: 'get',
                    url: IP + "contentType",
                    data: {
                        contractTypeId: that.contractType
                    },
                    success: function (res) {
                        if (res.code == 200) {
                            that.contentTypeList = res.data;
                        } else {
                            that.$Message.error({
                                content: '无法获取合同内容类别列表',
                                duration: 6
                            });
                        }
                    }
                });
            },
            deleteOperator: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: "提示",
                    content: "确定要删除此人吗？",
                    onOk: function () {
                        that.operatorList.splice(index, 1);
                    }
                });
            },
            addOperator: function () {
                var that = this;
                this.$Modal.confirm({
                    content: '<input class="layui-input" id="operator-name" placeholder="请输入名字"></input>',
                    onOk: function () {
                        var operatorName = $('#operator-name').val();
                        if (operatorName == null || operatorName == '') {
                            this.$Message.error({
                                content: '请输入姓名',
                                duration: 6
                            });
                        } else {
                            that.operatorList.push(operatorName)
                        }
                    }
                });
            },
            deleteSaler: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: "提示",
                    content: "确定要删除此人吗？",
                    onOk: function () {
                        var saler = that.salerShowList.splice(index, 1)[0];
                        console.log(saler);
                        that.salerList.push({
                            name:saler.name,
                            id:saler.sellerId
                        });
                    }
                });
            },
            addSaler: function () {
                var that = this;
                this.$Modal.confirm({
                    render(h) {
                        var salerList = that.salerList;
                        var optionList = [];
                        for (var i = 0; i < salerList.length; i++) {
                            var person = salerList[i];
                            var id = person.id;
                            var name = person.name;
                            optionList.push(h('Option', {
                                props: {
                                    value: id + "#" + name,
                                    key: id
                                }
                            }, name))
                        }
                        return h('Select', {
                            props: {
                                value: that.id
                            },
                            on: {
                                'on-change': function (val) {
                                    that.value = val
                                }
                            }
                        }, optionList)
                    },
                    onOk() {
                        if(that.value){
                            var v = that.value.split('#');
                            that.salerList.forEach((e,i) => {
                                if(e.id == parseInt(v[0], 10)){
                                    that.salerList.splice(i,1);
                                }
                            });
                            that.salerShowList.push({
                                sellerId: parseInt(v[0], 10),
                                name: v[1]
                            });
                            that.value = '';
                        }
                    }
                });
            },
            deleteTech: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: "提示",
                    content: "确定要删除此人吗？",
                    onOk: function () {
                        that.techList.splice(index, 1);
                    }
                });
            },
            addTech: function () {
                var that = this;
                this.$Modal.confirm({
                    content: '<input class="layui-input" id="operator-name" placeholder="请输入名字"></input>',
                    onOk: function () {
                        var name = $('#operator-name').val();
                        if (name == null || name == '') {
                            this.$Message.error({
                                content: '请输入姓名',
                                duration: 6
                            });
                        } else {
                            that.techList.push(name);
                        }
                    }
                });
            },
            create: function () {
                var that = this;
                var type;
                if(this.show == 0){
                    type = false;
                }else if(this.show == 1){
                    type = true;
                }

                var data = {
                    amount: parseFloat(that.amount), //合同总金额
                    block: parseInt(that.block), //区块id
                    content: that.content, //合同简介
                    contractType: parseInt(that.contractType), //合同类别id
                    contentType: parseInt(that.contractContentType), //合同内容类别id
                    createDate: that.contract_date, //创建日期
                    customer: that.customer, //客户
                    signDate: that.changeTS2ST(that.sign_date), //合同签订日期
                    name: that.name, //合同名称
                    operator: that.operatorList.join(','), //经办人
                    technician: that.techList.join(','), //技术服务员
                    operationTag: type, //是否为运维合同
                    contractSellerList: that.salerShowList, //销售员id
                }
                //根据不同合同类别添加字段
                if (!type) {
                    //合同节点list
                    data.contractNodeList = [{
                            "money": parseFloat(that.earnest) || 0,
                            "name": "定金"
                        },
                        {
                            "money": parseFloat(that.received) || 0,
                            "name": "预收款"
                        },
                        {
                            "money": parseFloat(that.progress) || 0,
                            "name": "进度款"
                        },
                        {
                            "money": parseFloat(that.delivery) || 0,
                            "name": "发货款"
                        },
                        {
                            "money": parseFloat(that.acceptance) || 0,
                            "name": "验收款"
                        },
                        {
                            "money": parseFloat(that.guarantee) || 0,
                            "name": "质保金"
                        },
                    ];
                } else {
                    //运维合同服务开始时间
                    data.serviceStart = that.changeTS2ST(that.service_start);
                    //运维合同服务结束时间
                    data.serviceEnd = that.changeTS2ST(that.service_end);
                    //合同到期日期
                    data.due = that.changeTS2ST(that.due);
                }

                //校验
                if (that.contractType == 0) {
                    that.$Message.error({
                        content: '请选择合同类别',
                        duration: 6
                    });
                } else if(that.contractContentType == 0){
                    that.$Message.error({
                        content: '请选择合同内容类别',
                        duration: 6
                    });
                } else if(that.name == ''){
                    that.$Message.error({
                        content: '请输入合同名称',
                        duration: 6
                    });
                } else if(that.block == 0){
                    that.$Message.error({
                        content: '请选择区块',
                        duration: 6
                    });
                } else if(that.operatorList.length == 0){
                    that.$Message.error({
                        content: '请填写经办人',
                        duration: 6
                    });
                } else if(that.salerShowList.length == 0){
                    that.$Message.error({
                        content: '请填写销售业务员',
                        duration: 6
                    });
                } else if(that.techList.length == 0){
                    that.$Message.error({
                        content: '请填写技术服务员',
                        duration: 6
                    });
                } else if(that.customer == ''){
                    that.$Message.error({
                        content: '请输入客户名称',
                        duration: 6
                    });
                } else if(that.sign_date == ''){
                    that.$Message.error({
                        content: '请选择合同签订日期',
                        duration: 6
                    });
                } else if(type && that.due == ''){
                    that.$Message.error({
                        content: '请选择合同到期日期',
                        duration: 6
                    });
                } else if(that.amount == 0){
                    that.$Message.error({
                        content: '合同金额不应为0',
                        duration: 6
                    });
                } else if(!type && that.amount !=  (that.earnest + that.received + that.progress + that.delivery + that.acceptance + that.guarantee)){
                    that.$Message.error({
                        content: '请检查合同节点金额，总合不等于合同额',
                        duration: 6
                    });
                } else {
                    //请求
                    $.ajax({
                        type: 'POST',
                        url: IP + "contract",
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function (res) {
                            if (res.code == 200) {
                                that.$Message.info({
                                    content: '创建合同成功',
                                    duration: 6
                                });
                                window.location.href = IP_ + 'user/htmlPages/contractList.html';
                            } else {
                                that.$Message.error({
                                    content: '无法新建合同',
                                    duration: 6
                                });
                            }
                        },
                        error: function (res) {
                            console.log(res);
                            that.$Message.error({
                                content: '无法新建合同',
                                duration: 6
                            });
                        }
                    });
                }

            },
            reset: function () {
                this.contractType = 0;
                this.contractContentType = 0;
                this.block = 0;
                this.operatorList = [];
                this.salerShowList = [];
                this.techList = [];
                this.name = ''; //合同名
                this.customer = ''; //客户名称
                this.sign_date = ''; //合同签订日期
                this.due = ''; //合同到期日期
                this.amount = 0; //合同金额
                this.earnest = 0; //定金
                this.received = 0; //预收款
                this.progress = 0; //进度款
                this.delivery = 0; //发货款
                this.acceptance = 0; //验收款
                this.guarantee = 0; //质保金
                this.service_start = ''; //合同节点时间
                this.service_end = ''; //合同节点时间
                this.content = ''; //合同简介
            },
        }
    });
    global.createBody = createBody;
})(window);