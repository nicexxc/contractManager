(function (global) {
    /**
     * 归属区块
     */
    var contractPlace = new Vue({
        el: "#contract-place",
        data: {
            list: [
                /*{
                    "id": 0,
                    "name": "",
                    "tag": ""
                }*/
            ],
            place: 0
        },
        beforeMount: function () {
            var that = this;
            $.ajax({
                type: 'get',
                url: IP + "block",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.list = res.data;
                        that.list.unshift({
                            "id": 0,
                            "name": "全部",
                            "tag": ""
                        });
                    } else {
                        that.$Message.error({
                            content: '无法获取区块列表',
                            duration: 6
                        });
                    }
                }
            });
        },
        methods: {
            flash: function () {
                $('#search-btn').click();
            }
        }
    });
    //向外暴露
    global.contractPlace = contractPlace;

    /**
     * 合同类别
     * 
     */
    var contractType = new Vue({
        el: "#contract-type",
        data: {
            contractTypeList: [
                /*{
                    "id": 0,
                    "name": "",
                    "tag": ""
                }*/
            ],
            contractType: 0,
        },
        beforeMount: function () {
            var that = this;
            $.ajax({
                type: 'get',
                url: IP + "contractType",
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        that.contractTypeList = res.data;
                        that.contractTypeList.unshift({
                            "id": 0,
                            "name": "全部",
                            "tag": ""
                        });
                    } else {
                        that.$Message.error({
                            content: '无法获取合同类别列表',
                            duration: 6
                        });
                    }
                }
            });
        },
        methods: {
            flash: function () {
                $('#search-btn').click();
            }
        }
    });
    //向外暴露
    global.contractType = contractType;

    /**
     * 归档状态
     */
    var fileState = new Vue({
        el:'#search-state',
        data:{
            state:0
        },
        methods:{
            flash:function () {
                $('#search-btn').click();
            }
        }
    });
    //向外暴露
    global.fileState = fileState;
})(window);