(function (global) {
    //仅仅作为initialMap的说明作用，无其他实际用途
    var map = {
        1: "经理",
        2: "财务",
        3: "总负责人",
        4: "地区负责人",
        5: "销售",
        6: "采购",
        7: "地区操作员",
    }
    /**
     * 判断该用户是否具备auth中的权限
     * 
     * @param {Array} auth 权限列表
     */
    global.authJudge = function (auth) {
        var initialMap = {
            "manager": 1,
            "financialOfficer": 2,
            "cheif": 3,
            "areaCheif": 4,
            "saler": 5,
            "purchaser": 6,
            "operator": 7
        }
        var authArray = new Array(initialMap.length);
        auth.forEach(function (e) {
            authArray[initialMap[e]] = 1;
        });
        return function (roleNumber) {
            if (authArray[roleNumber] == 1) {
                return true;
            } else {
                return false;
            }
        }
    }
    var hasError = false; // 防止由于ajax请求结果为多个401而导致弹出多个确认框
    $.ajaxSetup({
        complete: function (xhr, status) {
            var d = eval('(' + xhr.responseText + ')');
            if (d.code == 401) {
                if (!hasError) {
                    hasError = true;
                    alert(d.msg);
                    window.location.href = IP_ + 'login.html';
                }
            } else if (d.code == 403) {
                alert(d.msg);
            }

        }
    });

    //请求未读消息数量
    $.ajax({
        type: 'GET',
        url: IP + 'staff/hh',
        data: {},
        success: function (res) {
            
        }
    });
})(window)