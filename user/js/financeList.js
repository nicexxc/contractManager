layui.use(['element', 'layer', 'table', 'laydate'], function () {
    var element = layui.element;
    var layer = layui.layer;
    var table = layui.table;
    var laydate = layui.laydate;
    var role = $.cookie('role');
    var judgeOpe = authJudge(['manager', 'financialOfficer', 'areaCheif', 'operator']);
    var judgeArea = authJudge(['areaCheif', 'operator']);
    var operationReceive = (judgeOpe(role) ? '#operation-receive' : '#operation-receive-inferior');
    var operationBill = (judgeOpe(role) ? '#operation-bill' : '#operation-bill-inferior');
    var judgeAdmin = authJudge(['manager', 'financialOfficer', 'cheif']);

    table.render({
        elem: '#finance-receive',
        id: 'FINANCE_RECEIVE',
        url: IP + 'accounts',
        toolbar: judgeAdmin(role)?'#toolbar-receive':'',
        //,defaultToolbar: 'filter'
        title: '合同列表',
        totalRow: true,
        request: {
            pageName: 'pageNum', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        where: {
            startDate: '',
            endDate: '',
            key: ''
        },
        parseData: function (res) {
            return {
                "code": (res.code = 200 ? 0 : 1), //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.all_record, //解析数据长度
                "data": res.data.data //解析数据列表
            };
        },
        cols: [
            [
                {
                    field: 'number',
                    title: '合同编号',
                    fixed: 'left'
                },
                {
                    field: 'name',
                    title: '合同名称'
                },
                {
                    field: 'block',
                    title: '归属区块',
                    sort: true
                },
                {
                    field: 'contract_type',
                    title: '合同类别',
                    sort: true
                },
                {
                    title: '收款阶段',
                    templet: '#nodeShowTemp'
                },
                {
                    field: 'accountReceivable',
                    title: '当期应收款',
                    sort: true
                },
                {
                    field: 'accountActual',
                    title: '当期实收款',
                    sort: true
                },
                {
                    field: 'status',
                    title: '状态',
                    templet: '#stateTpl'
                },
                {
                    fixed: 'right',
                    title: '操作',
                    toolbar: operationReceive,
                    minWidth: 170
                },
            ]
        ],
        page: true,
        // ,data:data
        // ,height: 'full-50'
    });

    table.render({
        elem: '#finance-bill',
        url: IP + 'api/v1/actural/ticket_list',
        toolbar: judgeAdmin(role)?'#toolbar-bill':'',
        //,defaultToolbar: 'filter'
        id: 'FINANCE_BILL',
        title: '合同列表',
        totalRow: true,
        request: { //undo
            pageName: 'page_num', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        parseData: function (res) {
            return {
                "code": (res.code = 200 ? 0 : 1), //解析接口状态
                // "msg": res.message, //解析提示文本
                "count": res.data.total_record, //解析数据长度
                "data": (res.data?res.data.data :[])//解析数据列表
            };
        },
        cols: [
            [
                {
                    field: 'contract_number',
                    title: '合同编号',
                    fixed: 'left',
                    sort: true
                },
                {
                    field: 'contract_name',
                    title: '合同名称'
                },
                {
                    field: 'block_name',
                    title: '归属区块',
                    sort: true
                },
                {
                    // templet: '#stageTml',
                    field: 'issue',
                    title: '期数'
                },
                {
                    field: 'should',
                    title: '应开票',
                    sort: true
                },
                {
                    field: 'actural',
                    title: '实开票',
                    sort: true
                },
                {
                    title: '状态',
                    templet: '#stateTplBill'
                },
                {
                    fixed: 'right',
                    title: '操作',
                    toolbar: operationBill,
                    minWidth: 170
                },
            ]
        ],
        page: true,
        // ,data:data
        // ,height: 'full-50'
    });

    if (percentageShow.percentageShow) {
        table.render({
            elem: '#finance-percentages',
            url: IP + 'commission',
            // ,toolbar: '#Tabletoolbar'
            //,defaultToolbar: 'filter'
            id: 'FINANCE_PERCENTAGE',
            title: '合同列表',
            totalRow: true,
            request: {
                pageName: 'pageNum', //页码的参数名称，默认：page
                limitName: 'record' //每页数据量的参数名，默认：limit
            },
            where: {
                startTime: '',
                endTime: '',
                key: ''
            },
            parseData: function (res) {
                return {
                    "code": (res.code = 200 ? 0 : 1), //解析接口状态
                    // "msg": res.message, //解析提示文本
                    "count": res.data.totalRecords, //解析数据长度
                    "data": res.data.arr //解析数据列表
                };
            },
            cols: [
                [{
                        field: 'number',
                        title: '合同编号',
                        fixed: 'left',
                        sort: true
                    },
                    {
                        field: 'signDate',
                        title: '签订日期',
                        sort: true
                    },
                    {
                        field: 'contractName',
                        title: '合同名称'
                    },
                    {
                        field: 'block',
                        title: '归属区块',
                        sort: true
                    },
                    {
                        field: 'money',
                        title: '应支付',
                        sort: true
                    },
                    {
                        field: 'saler',
                        title: '销售业务员'
                    },
                    {
                        field: 'status',
                        title: '状态',
                        templet: '#stateTplPc'
                    },
                    {
                        fixed: 'right',
                        title: '操作',
                        templet: judgeArea(role) ? '' : '#stateTplPcOperation',
                        minWidth: 170
                    },
                ]
            ],
            page: true,
            // ,data:data
            // ,height: 'full-50'
        });
    }


    $('#search-btn').click(function () {
        var id = '';
        var v = {
            key: $('#search-content').val(),
            startDate: searchDate.changeTS2ST(searchDate.start) || '',
            endDate: searchDate.changeTS2ST(searchDate.end) || '',
        }
        var where = {};
        var tab = $('.layui-tab-title>li');
        if (tab.eq(0).hasClass('layui-this')) {
            id = 'FINANCE_RECEIVE';
            where = {
                key: v.key,
                startDate: v.startDate,
                endDate: v.endDate
            }
        } else if (tab.eq(1).hasClass('layui-this')) {
            id = 'FINANCE_BILL';
            where = {
                key: v.key,
                start_date: v.startDate,
                end_date: v.endDate
            }
        } else {
            id = 'FINANCE_PERCENTAGE';
            where = {
                key: v.key,
                startTime: v.startDate,
                endTime: v.endDate
            }
        }
        table.reload(id, { //此处是上文提到的 初始化标识id
            where: where
        });
    });

    /*
    表格操作栏事件
    还需添加向服务器端发送查看、删除、编辑、归档合同的请求
     */
    table.on('tool(finance-receive)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象

        if (layEvent === 'detail') { //查看
            window.location.href = 'financedetailinfo.html?id=' + data.contractId + '&tag=' + (data.contract_tag == 1 ? true : false) + '&from=r';
        } else if (layEvent === 'edit-receive') { //编辑收款
            //do something
            window.location.href = './editFinanceInfo.html?id=' + data.contractId + '&tag=' + data.contract_tag + '&tab=' + 0 + '&name=' + data.name;
        }
    });

    table.on('tool(finance-bill)', function (obj) {
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        if (layEvent === 'edit') { //添加支付信息
            window.location.href = './editFinanceInfo.html?id=' + data.contract_id + '&tag=' + (data.contract_tag ? 1 : 0) + '&tab=' + 1 + '&name=' + data.contract_name;
        } else if (layEvent === 'detail') {
            window.location.href = 'financedetailinfo.html?id=' + data.contract_id + '&tag=' + data.contract_tag + '&from=b';
        }
    });

    table.on('tool(finance-percentages)', function (obj) {
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        if (layEvent === 'review-percentages') { //添加支付信息
            percentageModal.showModal(data.id);//percentageModal位于financeListVue.js
        }
    });

    table.on('toolbar(finance-receive)', function (obj) {
        exportInfo(obj);
    });

    table.on('toolbar(finance-bill)', function (obj) {
        exportInfo(obj);
    });

    function exportInfo(obj) {
        var event = obj.event;
        if (event === 'export') {
            exportModal.show = true;//exportModal位于financeListVue.js
        }
    }
});