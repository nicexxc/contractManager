var that = this;

layui.use(['element', 'layer', 'table', 'form', 'laydate'], function () {
    var element = layui.element;
    var layer = layui.layer;
    var table = layui.table;
    var form = layui.form;
    var laydate = layui.laydate;
    var role = $.cookie('role');
    var operationBar = '';
    var judgeAdmin = authJudge(['manager', 'financialOfficer', 'cheif']);
    var judgeArea = authJudge(['areaCheif', 'operator']);
    if (judgeAdmin(role)) {
        operationBar = '#superiorBar';
    } else if (judgeArea(role)) {
        operationBar = '#inferiorBar';
    } else {
        operationBar = '#lowestBar';
    }

    var toolbar = '';
    if (judgeAdmin(role)||judgeArea(role)) {
        toolbar = '#Tabletoolbar'
    } else {
        toolbar = ''
    }

    form.render();

    function where() {
        var data = {
            key: $('#search-content').val(),
            start_date: searchDate.changeTS2ST(searchDate.start) || '',
            end_date: searchDate.changeTS2ST(searchDate.end) || '',
            block_id: parseInt(contractPlace.place),
            type_id: parseInt(contractType.contractType),
            archive_tag: parseInt(fileState.state)
        }
        return data;
    }

    table.render({
        elem: '#contrastListTable',
        id: 'CONTRACT_LIST',
        url: IP + 'contract_list',
        toolbar: toolbar,
        //defaultToolbar: 'filter',
        request: {
            pageName: 'page_num', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        where: where(),
        parseData: function (res) {
            //保持选中状态
            // var data = res.data ? res.data.data : [];
            // data.forEach((e,index) => {
            //     if(exportModal.contractList.has(e.id)){
            //         data[index].LAY_CHECKED = true;
            //     }
            // });
            //==========
            return {
                "code": (res.code = 200 ? 0 : 1), //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": (res.data ? res.data.all_record : 0), //解析数据长度
                "data": (res.data ? res.data.data : []) //解析数据列表
            };
        },
        title: '合同列表',
        totalRow: true,
        cols: [
            [
                /*{
                    type: 'checkbox',
                    fixed: 'left'
                },*/
                {
                    field: 'number',
                    title: '合同编号',
                    fixed: 'left',
                    width: "10%"
                },
                {
                    field: 'name',
                    title: '合同名称'
                },
                {
                    field: 'contract_type',
                    title: '合同类别'
                },
                {
                    field: 'block',
                    title: '归属区块'
                },
                {
                    field: 'sign_date',
                    title: '签订日期',
                    sort: true
                },
                {
                    fixed: 'right',
                    title: '操作',
                    toolbar: operationBar,
                    unresize: true,
                    minWidth: 240
                },
            ]
        ],
        page: true,
        // ,data:data
        // ,height: 'full-50'
    });
    $('#search-btn').click(function () {
        table.reload("CONTRACT_LIST", { //此处是上文提到的 初始化标识id
            where: where()
        });
    });
    /*
    表格操作栏事件
    还需添加向服务器端发送查看、删除、编辑、归档合同的请求
     */
    table.on('tool(contrastListTable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象

        if (layEvent === 'detail') { //查看
            window.location.href = './readContract.html?id=' + data.id + '&tag=' + data.contract_tag;
        } else if (layEvent === 'del') { //删除
            layer.confirm('您确定要删除此份合同吗？', function (index) {
                //向服务端发送删除指令
                $.ajax({
                    type: 'DELETE',
                    url: IP + 'contract?ContractId=' + data.id,
                    data: {},
                    success: function (res) {
                        if (res.code == 200) {
                            obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                            layer.close(index);
                        } else {
                            layer.msg('无法删除此项目', {
                                time: 2000, //2s后自动关闭
                            });
                        }
                    }
                });
            });
        } else if (layEvent === 'edit') { //编辑
            if (judgeAdmin(role)) {
                window.location.href = './editPage.html?id=' + data.id + '&tag=' + data.contract_tag;
            }
        } else if (layEvent === 'node') { //追加节点
            contractAddNote.id = data.id; //contractAddNote位于contractListVue.js中
            contractAddNote.getList();
            $("#add-node").fadeIn();
            $(".mash").fadeIn();
        }
    });

    table.on('toolbar(contrastListTable)', function (obj) {
        var event = obj.event;
        if (event === 'export') {
            exportModal.show = true;//exportModal位于contractListVue.js中
        }
    });

    // table.on('checkbox(contrastListTable)', function (obj) {
    //     var list = exportModal.contractList;
    //     var id = obj.data.id;
    //     if(list.has(id)){
    //         exportModal.contractList.delete(id);
    //     }else{
    //         exportModal.contractList.add(id);
    //     }
    // });
});