(function (global) {
    var body = new Vue({
        el: '#contract-body',
        data: {
            fileId: 0,
            contractId: 0, //合同ID
            contractName: '', //合同名
            contractNum: '', //合同编号
            archiveDate: '', //归档日期
            fileNum: '', //归档号
            cabinetNum: '', //档案柜号
            writeMan: '', //数据录入员
            curator: '', //保管员
            remark: '', //备注
            contractFiles: [],
            enclosureFiles: [],
            contract_contents:'',
            others:''
        },
        beforeMount() {
            var that = this;
            var id = window.location.href.split('?')[1].split('=')[1];
            this.contractId = parseInt(id, 10);
            $.ajax({
                url: IP + 'show_archive',
                type: 'GET',
                data: {
                    contract_id: this.contractId
                },
                success: function (res) {
                    var d = res.data;
                    if (res.code == 200) {
                        if(d.contract_contents){
                            var CCNames = d.contract_contents.split(',');
                        }
                        if(d.contract_contents){
                            var OTNames = d.others.split(',');
                        }
                        that.contractName = d.contract_name;
                        that.fileId = d.archive_id;
                        that.contractNum = d.contract_number;
                        that.writeMan = d.operator;
                        that.curator = d.clerk;
                        that.fileNum = d.archive_number;
                        that.archiveDate = d.archive_date;
                        that.cabinetNum = d.position_number;
                        that.remark = d.comment;
                        that.contract_contents = d.contract_contents;
                        that.others = d.others;
                        CCNames.forEach(e => {
                            that.contractFiles.push(e);
                        });
                        OTNames.forEach(e => {
                            that.enclosureFiles.push(e);
                        });
                    } else {
                        that.$Message.error({
                            content: res.msg,
                            duration: 6
                        });
                    }
                },
                error: function (res) {
                    console.log(res);
                    that.$Message.error({
                        content: '获取归档信息失败',
                        duration: 6
                    });
                }
            });
        },
        methods: {
            /**
             * 时间转换
             * @param {timestamp} ts js默认时间格式
             */
            changeTS2ST: function (ts) {
                var date;
                if (ts) {
                    date = new Date(ts);
                    return date.getFullYear() + '-' + ((date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-' + ((date.getDate()) >= 10 ? (date.getDate()) : '0' + (date.getDate()));
                }
            },
            deleteContractFile: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: '是否删除这个文件？',
                    onOk: function () {
                        that.contractFiles.splice(index, 1);
                    }
                });
            },
            addToContractContainer: function (name) {
                this.contractFiles.push(name);
            },
            deleteEnclosureFile: function (index) {
                var that = this;
                this.$Modal.confirm({
                    title: '是否删除这个文件？',
                    onOk: function () {
                        that.enclosureFiles.splice(index, 1);
                    }
                });
            },
            addToEnclosureContainer: function (name) {
                this.enclosureFiles.push(name);
            },
            fileIt: function () {
                var that = this;
                this.$Modal.confirm({
                    title: '确定编辑该合同的归档消息吗？',
                    onOk: function () {
                        if (that.cabinetNum == '') {
                            that.$Message.error({
                                content: '请填写档案柜号',
                                duration: 6
                            });
                        } else if (that.curator == '') {
                            that.$Message.error({
                                content: '请填写保管员',
                                duration: 6
                            });
                        } else {
                            var formData = new FormData();
                            formData.append('id', that.fileId);
                            formData.append('contractId', that.contractId);
                            formData.append('clerk', that.curator);
                            formData.append('positionNumber', that.cabinetNum);
                            formData.append('comment', that.remark);
                            formData.append('archiveDate',that.changeTS2ST(that.archiveDate));
                            formData.append('contractContents',that.contractFiles.join(','));
                            formData.append('others',that.enclosureFiles.join(','));
                            global.$.ajax({
                                url: IP + 'archive_contract',
                                type: 'POST',
                                data: formData,
                                processData: false,
                                contentType: false,
                                mimeType: 'multipart/form-data',
                                success: function (res) {
                                    var res = eval("(" + res + ")")
                                    if (res.code == 200) {
                                        global.location.href = IP_ + 'user/htmlPages/fileContractList.html';
                                    } else {
                                        that.$Message.error({
                                            content: res.msg,
                                            duration: 6
                                        });
                                    }
                                },
                                error: function (res) {
                                    console.log(res);
                                    that.$Message.error({
                                        content: '归档失败',
                                        duration: 6
                                    });
                                }
                            });
                        }
                    }
                });
            },
            cancel: function () {
                this.$Modal.confirm({
                    title: '确定取消归档吗？',
                    onOk: function () {
                        global.location.href = IP_ + 'user/htmlPages/fileContractList.html';
                    }
                });
            }
        },
    });
    global.body = body;
})(window)