layui.use(['element', 'layer', 'table', 'form'], function () {
    var element = layui.element;
    var layer = layui.layer;
    var table = layui.table;
    var form = layui.form;
    form.render();

    var contract_id;
    var tag;//合同类型判断，true运维，false普通

    var params = window.location.href.split("?")[1];
    if (params) {
        var paramList = params.split('&');
        contract_id = paramList[0].split("=")[1];
        tag = paramList[1].split("=")[1];
        var from = (paramList[2] || '').split("=")[1];
        if(from == 'm'){
            bar.tag = false; //请在对应的financedetailinfoVue.js中寻找bar
            navigator.page = 4;
        } else if(from == 'r') {
            bar.switchTab = false;
        } else if(from == 'b') {
            bar.switchTab = true;
        } else{
            navigator.page = 2;
        }
    }
    $('.filecontrastListTable').css('display', 'none');

    var titleArray = tag=="true" ? [{
            field: 'nodeName',
            title: '节点名称'
        },
        {
            field: 'receivableDate',
            title: '应收款时间'
        },
        {
            field: 'receivableMoney',
            title: '应收款金额'
        },
        {
            field:'actualMoney',
            title:'实收款金额'
        },
        {
            templet: '#stateTpl',
            title: '状态'
        },
    ] : [{
            field: 'nodeName',
            title: '节点名称'
        },
        {
            field: 'stage',
            title: '节点期数'
        },
        {
            field: 'receivableMoney',
            title: '应收款金额'
        },
        {
            field: 'actualMoney',
            title: '实收款金额'
        },
        {
            field: 'receivableDate',
            title: '应收款时间'
        },
        {
            templet: '#stateTpl',
            title: '状态'
        },
    ]
    table.render({
        elem: '#collection1',
        url: IP + 'accounts/receivable/' + contract_id,
        // toolbar: '#toolbar-collection1',
        //defaultToolbar: 'filter',
        title: '收款信息',
        totalRow: true,
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": (res.code == 200 ? 0 : 1),
                "msg": res.msg,
                "count": res.data.all_record,
                "data": res.data.data
            };
        },
        request: {
            pageName: 'pageNum', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        cols: [
            titleArray
        ],
        page: true,
        // ,data:data
        // ,height: 'full-50'
        done: function (res, curr, count) {
            if (tag != "true") {
                merge(res,['nodeName']);
            }
        }
    });
    table.render({
        elem: '#collection2',
        url: IP + 'accounts/actual/' + contract_id,
        // toolbar: '#toolbar-collection2',
        //defaultToolbar: 'filter',
        title: '收款信息',
        totalRow: true,
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": (res.code == 200 ? 0 : 1),
                "msg": res.msg,
                "count": res.data.all_record,
                "data": res.data.data
            };
        },
        request: {
            pageName: 'pageNum', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        cols: [
            [{
                    field: 'actualDate',
                    title: '实收款时间'
                },
                {
                    field: 'actualAmount',
                    title: '实收款金额'
                },
                {
                    field: 'remark',
                    title: '备注'
                },
            ]
        ],
        page: true,
        // ,data:data
        // ,height: 'full-50'
    });

    table.render({
        elem: '#invoice1',
        url: IP + 'api/v1/actural/ticket_detail',
        // toolbar: '#toolbar-contractinfo2',
        //defaultToolbar: 'filter',
        title: '开票情况',
        totalRow: true,
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": (res.code == 200 ? 0 : 1),
                "msg": res.msg,
                "count": res.data.all_record,
                "data": res.data.data
            };
        },
        where:{
            contract_id:contract_id
        },
        request: {
            pageName: 'page_num', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        cols: [
            [{
                    field: 'should_money',
                    title: '应开票金额'
                },
                {
                    field: 'should_date',
                    title: '应开票时间'
                },
                {
                    field:'issue_id',
                    title:'应开票期数'
                },
                {
                    field: 'actural_money',
                    title: '实开票金额'
                },
                {
                    templet: '#stateBillTpl',
                    title: '状态'
                },

            ]
        ],
        page: true,
        // ,data:data
        // ,height: 'full-50'
    });

    table.render({
        elem: '#invoice2',
        url: IP + 'api/v1/actural/ticket_actural_detail',
        // toolbar: '#toolbar-contractinfo2',
        //defaultToolbar: 'filter',
        title: '开票情况',
        totalRow: true,
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": (res.code == 200 ? 0 : 1),
                "msg": res.msg,
                "count": res.data.all_record,
                "data": res.data.data
            };
        },
        where:{
            contract_id:contract_id
        },
        request: {
            pageName: 'page_num', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        cols: [
            [{
                    field: 'date',
                    title: '实开票时间'
                },
                {
                    field: 'money',
                    title: '实开票金额'
                },
                {
                    field: 'remark',
                    title: '备注'
                },

            ]
        ],
        page: true,
        // ,data:data
        // ,height: 'full-50'
    });

    function merge(res,arr) { //合并单元格

        var data = res.data;
        var mergeIndex = 0; //定位需要添加合并属性的行数
        var mark = 1; //这里涉及到简单的运算，mark是计算每次需要合并的格子数
        var columsName = arr; //需要合并的列名称
        var columsIndex = [0, 1]; //需要合并的列索引值

        for (var k = 0; k < columsName.length; k++) { //这里循环所有要合并的列
            var trArr = $(".layui-table-body>.layui-table").find("tr"); //所有行
            for (var i = 1; i < res.data.length; i++) { //这里循环表格当前的数据
                var tdCurArr = trArr.eq(i).find("td").eq(columsIndex[k]); //获取当前行的当前列
                var tdPreArr = trArr.eq(mergeIndex).find("td").eq(columsIndex[k]); //获取相同列的第一列

                if (data[i][columsName[k]] === data[i - 1][columsName[k]]) { //后一行的值与前一行的值做比较，相同就需要合并
                    mark += 1;
                    tdPreArr.each(function () { //相同列的第一列增加rowspan属性
                        $(this).attr("rowspan", mark);
                    });
                    tdCurArr.each(function () { //当前行隐藏
                        $(this).css("display", "none");
                    });
                } else {
                    mergeIndex = i;
                    mark = 1; //一旦前后两行的值不一样了，那么需要合并的格子数mark就需要重新计算
                }
            }
            mergeIndex = 0;
            mark = 1;
        }
    }
});