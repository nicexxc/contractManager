layui.use(['element', 'layer', 'form'], function () {
    var element = layui.element;
    var layer = layui.layer;
    var form = layui.form;
    form.render();
});
$(function () {
    C1 = window.location.href.split("?")[1];
    if (C1) {
        contract_id = C1.split("=")[1];
        console.log(contract_id);
    }
    $('.filecontrastListTable').css('display', 'none');
    console.log(contract_id);
    $.ajax({
        type: "GET",
        url: IP + "show_archive",
        data: {
            contract_id
        },
        // dataType: "json",
        // contentType: "application/json",
        success: function (res) {
            var sh = [];
            // console.log(res);
            sh = res.data;
            console.log(sh);
            console.log(sh.clerk);
            var filem = '<div>合同名称：' + sh.contract_name + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;合同编号：' +
                sh.contract_number + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创建日期：' + sh.archive_date + '</div>';
            $("#filemess").html(filem);
            $("#filenumber1").html(sh.position_number);
            $("#filenum1").html(sh.archive_number);
            $("#username1").html(sh.operator);
            $("#storekeeper1").html(sh.clerk);
            // console.log(sh.contract_contents);
            var str = sh.contract_contents;
            // console.log(str);
            var arr = str?str.split(","):[];
            // console.log(arr);
            // console.log(arr[0]);
            // console.log(arr.length);
            var content111 = "";
            for (var i = 0; i < arr.length; i++) {
                content111 += '<a target="_blank" download="'+arr[i]+'" href="'+IP+'/download?fileName='+arr[i]+'"><span style="color: #2d8cf0;">' + arr[i] + '</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>';
            }
            $("#content1").html(content111);
            var str11 = sh.others;
            var arr11 = str11?str11.split(","):[];
            var content222 = "";
            for (var i = 0; i < arr11.length; i++) {
                content222 += '<a target="_blank" download="'+arr11[i]+'" href="'+IP+'/download?fileName='+arr11[i]+'"><span style="color: #2d8cf0;">' + arr11[i] + '</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>';
            }
            $("#content2").html(content222);
            $("#remarks1").html(sh.comment);
            // archive_number
        },
        async: false
    });
});