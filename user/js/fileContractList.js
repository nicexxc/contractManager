var that = this;
layui.use(['element', 'layer', 'table', 'form', 'laydate'], function () {
    var element = layui.element;
    var layer = layui.layer;
    var table = layui.table;
    var form = layui.form;
    var laydate = layui.laydate;
    var power = $.cookie('power');
    var operationBar = '#operationBar';

    form.render();
    table.render({
        elem: '#filecontrastListTable',
        id: 'CONTRACT_LIST',
        url: IP + 'contract_list',
        // toolbar: '#Tabletoolbar',
        //defaultToolbar: 'filter',
        request: {
            pageName: 'page_num', //页码的参数名称，默认：page
            limitName: 'record' //每页数据量的参数名，默认：limit
        },
        where: {
            key: null,
            block_id: 0,
            type_id: 0,
            archive_tag:0,
            // start_date: '',
            // end_date: '',
        },
        parseData: function (res) {
            return {
                "code": (res.code = 200 ? 0 : 1), //解析接口状态
                // "msg": res.message, //解析提示文本
                "count": res.data.all_record, //解析数据长度
                "data": res.data.data //解析数据列表
            };
        },
        title: '合同列表',
        totalRow: true,
        cols: [
            [{
                    field: 'number',
                    title: '合同编号',
                    fixed: 'left',
                    width: "10%"
                },
                {
                    field: 'name',
                    title: '合同名称'
                },
                {
                    field: 'contract_type',
                    title: '合同类别'
                },
                {
                    field: 'block',
                    title: '归属区块'
                },
                {
                    field: 'sign_date',
                    title: '签订日期',
                    sort: true
                },
                {
                    fixed: 'right',
                    title: '操作',
                    toolbar: operationBar,
                    unresize: true,
                    minWidth: 200
                },
            ]
        ],
        page: true,
        // ,data:data
        // ,height: 'full-50'
    });
    $('#search-btn').click(function () {
        table.reload("CONTRACT_LIST", { //此处是上文提到的 初始化标识id
            where: {
                key: $('#search-content').val(),
                block_id: parseInt(contractPlace.place),
                type_id: parseInt(contractType.contractType),
                archive_tag:parseInt(fileState.state)
            }
        });
    });
    /*
    表格操作栏事件
    还需添加向服务器端发送查看、删除、编辑、归档合同的请求
     */
    table.on('tool(filecontrastListTable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象

        if (layEvent == 'detail') { //查看
            window.location.href = './readFileContract.html?id=' + data.id;
        } else if (layEvent == 'del') { //删除
            layer.confirm('您确定要删除该合同的归档信息吗？', function (index) {
                //向服务端发送删除指令
                $.ajax({
                    type: 'DELETE',
                    url: IP + 'delete_archive?contract_id=' + data.id,
                    data: {},
                    success: function (res) {
                        if (res.code == 200) {
                            table.reload('CONTRACT_LIST', {
                                where: {
                                    key: $('#search-content').val(),
                                    block_id: parseInt(contractPlace.place),
                                    type_id: parseInt(contractType.contractType)
                                }
                            })
                            layer.close(index);
                        } else {
                            layer.msg('无法删除此项目', {
                                time: 2000, //2s后自动关闭
                            });
                        }
                    }
                });
            });
        } else if (layEvent == 'edit') { //编辑
            window.location.href = './editFileContract.html?id=' + data.id;
        } else if (layEvent == 'dea') { //归档
            window.location.href = './placeonfile.html?id=' + data.id;
        } else {

        }
    });
});