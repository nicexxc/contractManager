(function (global) {
    var body = new Vue({
        el:'#body',
        data:{
            notPurchase:false,
        },
        beforeMount() {
            $.ajax({
                type: 'GET',
                url: IP + 'reminder/number',
                data: {},
                success: function (res) {
                    if (res.code == 200) {
                        var count = res.data.count;
                        $.cookie('unread',count,{ path: '/' });
                        $('#unread-bubble').html(count);
                    }
                }
            });
            var judgeSP = authJudge(['saler','purchaser']);
            if(!judgeSP(role)){
                this.notPurchase = true;
            }
        },
    });
})(window)