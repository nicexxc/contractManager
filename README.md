#### 设计要求
不要以线框图设计为总体样式设计蓝本，总体样式要求与原样式一致————侧边导航栏要求一致，要求每个页面都有面包屑导航，且面包屑导航要求具有层级结构，点击后需要跳转到父页面）
#### 每次改动记得及时提交代码到码云
#### 开发工具
推荐使用VSCode
