var id = $.cookie('id');

function FillDetailAjax() {
	$.ajax({
		type: "get",
		url: IP + "staff/" + id,
		data: {},
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		async: true,
		dataType: "json",
		contentType: "application/json",
		success: function(obj) {
			if(obj.code==200){
				console.log(obj)
				fillDetail(obj)
			}
			if(obj.code==401){
				window.location.href = IP_ + 'login.html'
			}
		},
		Error: function() {
			alert("服务器出错");
		}
	})
}

function fillDetail(obj) {
	$("#sex").html((obj.data.sex == 1?'男':'女'));
	$("#name").html(obj.data.name);
	$("#phone").html(obj.data.phone);
	$("#departMent").html(obj.data.departMent);
}
