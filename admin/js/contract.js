//构建表头
function buildTable() {
	var columns =[{"title":"编号"},{"title":"合同类别"},{"title":"对应编号"},{"title":""}];
	//给table加一个id 是二级目录的名字
	$('.mainTable').DataTable({
		responsive: true,
		searching: false,
		// "bSort":false,
		"columns":columns,
		"bLengthChange": false,
		"bRetrieve": false,
		"bFilter": false, //过滤功能
		"paging":false,
		"aLengthMenu":[15],
		"columnDefs": [ 
		{ "orderable": false, "targets": 1 },
		{ "orderable": false, "targets": 2 },
		{ "orderable": false, "targets": 3 },
		],
	});
}
//填充数据
function ContractTableDataAjax() {
	$.ajax({
		type: "get",
		url: IP+"contractType",
		data: {
		},
		async: true,
		dataType: "json",
		contentType: "application/json",
		xhrFields: {withCredentials: true},
      	crossDomain:true,
		success: function(obj) {
			console.log(obj);
			if(obj.code==200){
				setTableData(obj);
			}
			if(obj.code==401){
				window.location.href = IP_ + 'login.html'
			}
		},
		Error: function() {
			alert("服务器出错");
		}
	})
}
//填充数据具体操作
function setTableData(obj) {
	console.log(obj.data)
	$('#mainTable').dataTable().fnClearTable();
	$.each(obj.data, function(index, item) {
		$('#mainTable').dataTable().fnAddData([
			'<span>'+item.id+'</span>',
			'<span>'+item.name+'</span>',
			'<span>'+item.tag+'</span>',
			'<a href="typedetail.html?id='+item.id+'&name='+item.name+'" id="'+item.id+'"><button class="btn btn-warning" style="margin-left:30px">编辑</button></a><button class="btn btn-info btn_addContent" data-toggle="modal" data-target="#addContent" style="margin-left:30px">添加合同内容类别</button>',
			])
	});
}
//构建表头
function buildTable2() {
	var columns =[{"title":"编号"},{"title":"归属区块"},{"title":"对应编号"},{"title":"区块负责人"},{"title":"区块操作员"},{"title":""}];
	//给table加一个id 是二级目录的名字
	$('.mainTable2').DataTable({
		responsive: true,
		searching: false,
		// "bSort":false,
		"columns":columns,
		"bLengthChange": false,
		"bRetrieve": false,
		"bFilter": false, //过滤功能
		"paging":false,
		"aLengthMenu":[15],
		"columnDefs": [ 
		{ "orderable": false, "targets": 1 },
		{ "orderable": false, "targets": 2 },
		{ "orderable": false, "targets": 3 },
		{ "orderable": false, "targets": 4 },
		{ "orderable": false, "targets": 5 }
		],
	});
}
//填充数据
function ContractTableDataAjax2() {
	$.ajax({
		type: "get",
		url: IP+"block",
		data: {
		},
		async: true,
		dataType: "json",
		contentType: "application/json",
		xhrFields: {withCredentials: true},
      	crossDomain:true,
		success: function(obj) {
			console.log(obj);
			if(obj.code==200){
				setTableData2(obj);
			}
			if(obj.code==401){
				window.location.href = IP_ + 'login.html'
			}
		},
		Error: function() {
			alert("服务器出错");
		}
	})
}
//填充数据具体操作
function setTableData2(obj) {
	console.log(obj.data)
	$('#mainTable2').dataTable().fnClearTable();
	$.each(obj.data, function(index, item) {
		$('#mainTable2').dataTable().fnAddData([
			'<span>'+item.id+'</span>',
			'<span>'+item.name+'</span>',
			'<span>'+item.tag+'</span>',
			'<span>'+item.responser+'</span>',
			'<span>'+item.operator+'</span>',
			'<button class="btn btn-warning btn_addContent"  data-toggle="modal" data-target="#newblock" style="margin-left:20px">重命名</button><button class="btn btn-info btn_addContent"  data-toggle="modal" data-target="#addOwner" style="margin-left:30px">设置区块负责人</button><button class="btn btn-primary btn_addContent"  data-toggle="modal" data-target="#addOperator" style="margin-left:30px">设置区块操作员</button>',
			])
	});
}
//模态框
function alertContractModal(){
	var modals=
	'<!--新建合同类别-->\
	<div class="modal fade" id="addType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">新建合同类别</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-2 margin-bottom-15" style="text-align:center">\
		<label for="name" class="control-label">合同类别:</label>\
	</div>\
	<div class="col-md-8 margin-bottom-15">\
		<input type="text" class="form-control" id="typename" value="">\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer">\
    <button type="button" class="btn btn-primary addType">添加</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--新建区块-->\
	<div class="modal fade" id="addBlock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">新建归属区块</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-2 margin-bottom-15" style="text-align:center">\
		<label for="name" class="control-label">归属区块:</label>\
	</div>\
	<div class="col-md-8 margin-bottom-15">\
		<input type="text" class="form-control" id="blockname" value="">\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer">\
    <button type="button" class="btn btn-primary addBlock">添加</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--添加合同内容类别-->\
	<div class="modal fade" id="addContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">添加合同内容类别</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-3 margin-bottom-15" style="text-align:center">\
		<label for="name" class="control-label">合同内容类别:</label>\
	</div>\
	<div class="col-md-8 margin-bottom-15">\
		<input type="text" class="form-control" id="contentname" value="">\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer temp">\
    <button type="button" class="btn btn-primary addContent">添加</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--修改归属区块名称-->\
	<div class="modal fade" id="newblock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">修改归属区块名称</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-3 margin-bottom-15" style="text-align:center">\
		<label for="name" class="control-label">归属区块名称:</label>\
	</div>\
	<div class="col-md-8 margin-bottom-15">\
		<input type="text" class="form-control" id="newblockname" value="">\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer temp">\
    <button type="button" class="btn btn-primary  newblock">确实</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--设置区块负责人-->\
	<div class="modal fade" id="addOwner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">设置区块负责人</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-3 margin-bottom-15" style="text-align:center">\
		<label for="name" class="control-label">区块负责人:</label>\
	</div>\
	<div class="col-md-8 margin-bottom-15">\
		<select class="form-control" id="ownerlist" placeholder="请选择">\
		</select>\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer temp">\
    <button type="button" class="btn btn-primary addOwner">添加</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
	</div><!-- /.modal -->\
	</div>\
	<!--设置区块操作员-->\
	<div class="modal fade" id="addOperator" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">设置区块操作员</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-3 margin-bottom-15" style="text-align:center">\
		<label for="name" class="control-label">区块操作员:</label>\
	</div>\
	<div class="col-md-8 margin-bottom-15">\
		<select class="form-control" id="operatorlist" placeholder="请选择">\
		</select>\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer temp">\
    <button type="button" class="btn btn-primary addOperator">添加</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>';
	$('body').prepend(modals);

}

//绑定id
$(document).on('click','.btn_addContent',function(){
	var userid = $(this).parents('tr').children("td").eq(0).children("span").text();
	console.log(userid)
	$('.temp').attr('id', userid);
})

$(function() {
	buildTable();
	ContractTableDataAjax();
	buildTable2();
	ContractTableDataAjax2();
	alertContractModal()
	//获取区块负责人列表
	$.ajax({
		type: "get",
		url: IP + "staff/areachief",
		data: {},
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		async: true,
		dataType: "json",
		contentType: "application/json",
		success: function(obj) {
			if(obj.code==200){
				console.log(obj);
				if(obj.data==""){
					$('#ownerlist').append('<option disabled="disabled" selected="selected">无可选的区块负责人</option>')
				}else{
					$.each(obj.data, function(index, val) {
						$('#ownerlist').append('<option value="'+val.id+'">'+val.name+'</option>')
					});
				}
			}
			if(obj.code==401){
				window.location.href = IP_ + 'login.html'
			}
		},
		Error: function() {
			alert("服务器出错");
		}
	})
	//获取区块操作人列表
	$.ajax({
		type: "get",
		url: IP + "staff/operator",
		data: {},
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		async: true,
		dataType: "json",
		contentType: "application/json",
		success: function(obj) {
			if(obj.code==200){
				console.log(obj);
				if(obj.data==""){
					$('#operatorlist').append('<option disabled="disabled" selected="selected">无可选的区块操作人</option>')
				}else{
					$.each(obj.data, function(index, val) {
						$('#operatorlist').append('<option value="'+val.id+'">'+val.name+'</option>')
					});
				}
			}
			if(obj.code==401){
				window.location.href = IP_ + 'login.html'
			}
		},
		Error: function() {
			alert("服务器出错");
		}
	})
	$('.addType').click(function(event) {
		var typename = $('#typename').val();
		console.log(typename);
		if(typename==""){
			alert("还有信息未填写!");
		}else{
			$.ajax({
				type: "POST",
				url: IP+"contractType",
				data:{
					"typeName":typename,
				},
				async: true,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						ContractTableDataAjax();
						$('#addType').modal('hide')
						$('#addSuccess').modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
	
	});
	$('.addBlock').click(function(event) {
		var blockname = $('#blockname').val();
		console.log(blockname);
		if(blockname==""){
			alert("还有信息未填写!");
		}else{
			$.ajax({
				type: "POST",
				url: IP+"block",
				data:{
					"name":blockname,
				},
				async: true,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						ContractTableDataAjax2();
						$('#addBlock').modal('hide')
						$('#addSuccess').modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
	
	});
	$('.addContent').click(function(event) {
		var contentname = $('#contentname').val();
		var contractTypeId = $(this).parent('.temp').attr('id');
		console.log(contentname);
		if(contentname==""){
			alert("还有信息未填写!");
		}else{
			$.ajax({
				type: "POST",
				url: IP+"contentType",
				data:{
					"contractTypeId": contractTypeId,
					"name":contentname
				},
				async: true,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						ContractTableDataAjax2();
						$('#addContent').modal('hide')
						$('#addSuccess').modal('show')
					}
					if (data.code==501) {
						$('#newblock').modal('hide')
						$('#nameError2').modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
	
	});
	$('.newblock').click(function(event) {
		var newblockname = $('#newblockname').val();
		var blockid = $(this).parent('.temp').attr('id');
		console.log(newblockname);
		if(newblockname==""){
			alert("还有信息未填写!");
		}else{
			$.ajax({
				type: "put",
				url: IP+"block",
				data:{
					"id": blockid,
					"name":newblockname
				},
				async: true,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						ContractTableDataAjax2();
						$('#newblock').modal('hide')
						$('#saveSuccess').modal('show')
					}
					if (data.code==501) {
						$('#newblock').modal('hide')
						$('#nameError2').modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
	
	});
	$('.addOwner').click(function(event) {
		var ownerid =  $("#ownerlist option:selected").val();
		var blockid = $(this).parent('.temp').attr('id');
		console.log(ownerid);
		if(ownerid==""){
			alert("还有信息未填写!");
		}else{
			$.ajax({
				type: "GET",
				url: IP+"blockUser",
				data:{
					"id":blockid,
					"responser":ownerid
				},
				async: true,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						ContractTableDataAjax2();
						$('#addOwner').modal('hide')
						$('#setSuccess').modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
	
	});
	$('.addOperator').click(function(event) {
		var operatorid =  $("#operatorlist option:selected").val();
		var blockid = $(this).parent('.temp').attr('id');
		console.log(operatorid);
		if(operatorid==""){
			alert("还有信息未填写!");
		}else{
			$.ajax({
				type: "GET",
				url: IP+"blockOperator",
				data:{
					"id":blockid,
					"operator":operatorid
				},
				async: true,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						ContractTableDataAjax2();
						$('#addOperator').modal('hide')
						$('#setSuccess').modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
	
	});
	$("#newblock").on("hidden.bs.modal", function() {
		$('.form-control').val("");  
	});
	$("#addContent").on("hidden.bs.modal", function() {
		$('.form-control').val("");  
	});
	$("#addType").on("hidden.bs.modal", function() {
		$('.form-control').val("");  
	});
	$("#addBlock").on("hidden.bs.modal", function() {
		$('.form-control').val("");  
	});
})
