// 注册全局组件
Vue.component('footnav', {
  template: '\
  <footer class="templatemo-footer navbar-fixed-bottom">\
  <div class="templatemo-copyright"><p>地址：浙江省杭州市下沙高教园区学正街18号 <br>Copyright &copy;浙江工商大学ITObase</p></div></footer>'
})
Vue.component('topnav', {
  template: '<div class="navbar navbar-inverse" role="navigation">\
  <div class="navbar-header"><div class="logo"><img src="../images/logo.png"/><h1>合同管理系统</h1></div></div>\
  <div class="navbar-right logo"><ul class="nav navbar-nav pull-right">\
  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>管理员</span>\
  <span class="thumb-small"><img src="../images/admin.png"  class="img-circle"></span>\
  <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="info.html">查看个人信息</a></li><li><a href="login.html">退出</a></li></ul>\
  </li></ul></div></div>'
})
Vue.component('leftnav', {
  template: '<div class="navbar-collapse collapse templatemo-sidebar">\
  <ul class="templatemo-sidebar-menu">\
  <li class="active"> <a href="staff.html"><i class="fa fa-user"></i>员工管理</a></li>\
  <li> <a href="role.html"><i class="fa fa-cubes"></i>角色管理</a> </li>\
  <li> <a href="info.html"><i class="fa fa-cog"></i>设置</a></li></ul></div>'
})


// 创建根实例
new Vue({
  el: '#vue-global'
})