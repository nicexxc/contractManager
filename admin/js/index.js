var IP = '/api/'
var IP_ = '/'

var rolemap ={
	"1":"经理",
	"2":"财务",
	"3":"总负责人",
	"4":"地区负责人",
	"5":"销售业务员",
	"6":"采购",
	"7":"地区操作员"
}
var name = $.cookie('nickname');
function header(){
	var header = '<div class="navbar navbar-inverse" role="navigation">\
  	<div class="navbar-header"><div class="logo"><img src="../images/logo.png"/><h1>合同管理系统</h1></div></div>\
  	<div class="navbar-right logo"><ul class="nav navbar-nav pull-right">\
  	<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>'+name+'</span>\
  	<span class="thumb-small"><img src="../images/admin.png"  class="img-circle"></span>\
  	<b class="caret"></b></a><ul class="dropdown-menu"><li><a href="info.html">查看个人信息</a></li><li><a href="'+IP_+'user/htmlPages/contractList.html'+'">业务端</a></li><li id="exit-login"><a href="#">退出</a></li></ul>\
 	</li></ul></div></div>'

	$(".topnav").html(header);
}
function footer() {
	var footer ='<div class="templatemo-copyright">\
	<p>地址：浙江省杭州市下沙高教园区学正街18号 <br>Copyright &copy;浙江工商大学ITObase</p>\
	</div>';
	$(".footer").html(footer);
}
//模态框
function alertModal(){
var modals=
'<!--添加成功-->\
	<div class="modal fade tips" id="addSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-check-circle"></i>已添加!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--设置成功-->\
	<div class="modal fade tips" id="setSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-check-circle"></i>设置成功!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--修改成功-->\
	<div class="modal fade tips" id="saveSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-check-circle"></i>修改成功!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--账号重复-->\
	<div class="modal fade tips" id="accountError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>账号重复!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--姓名重复-->\
	<div class="modal fade tips" id="nameError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>姓名重复!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--名称重复-->\
	<div class="modal fade tips" id="nameError2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>名称重复!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--电话重复-->\
	<div class="modal fade tips" id="phoneError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>电话重复!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--密码为空-->\
	<div class="modal fade tips" id="passwordIsEmpty tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>密码不能为空!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--密码长度有误-->\
	<div class="modal fade tips" id="passwordLengthError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>密码长度应为6位到16位!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--两次输入的密码不一致-->\
	<div class="modal fade tips" id="passwordDifferError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>两次输入的密码不一致!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--原密码错误-->\
	<div class="modal fade tips" id="passwordError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>原密码错误!\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--重置密码-->\
	<div class="modal fade tips" id="resetPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<i class="fa fa-exclamation-circle"></i>您确定要重置密码吗?\
	</div>\
   	<div class="modal-footer temp">\
	<button type="button" class="btn btn-danger fa fa-check surePasswordYes">确认</button>\
	<button type="button" class="btn btn-primary fa fa-remove cancelPasswordYes" data-dismiss="modal">取消</button>\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--重置密码-->\
	<div class="modal fade tips" id="newPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
	<div class="modal-body">\
	<label for="password" class="control-label">重置后的密码:</label>\
	<span id="password2"></span>\
	</div>\
   	<div class="modal-footer temp">\
	<button type="button" class="btn btn-danger fa fa-check" data-dismiss="modal">确认</button>\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>';
	$('body').prepend(modals);
	
}
$(function(){
	header();
	footer();
	alertModal();
	$('#exit-login').click(function () {
		$.ajax({
            type:'get',
            url:IP+'logout',
            data:{},
			success:function (res) {
				if(res.code == 200){
					window.location.href = IP_ + 'login.html'
				}
			}
		})
	});
	$('#addSuccess').on('show.bs.modal', function () {
    	setTimeout(function(){$("#addSuccess").modal("hide")},1600);
    })
	$('#setSuccess').on('show.bs.modal', function () {
    	setTimeout(function(){$("#setSuccess").modal("hide")},1600);
    })
	$('#saveSuccess').on('show.bs.modal', function () {
    	setTimeout(function(){$("#saveSuccess").modal("hide")},1600);
    })
	$('#accountError').on('show.bs.modal', function () {
    	setTimeout(function(){$("#accountError").modal("hide")},1600);
    })
	$('#nameError').on('show.bs.modal', function () {
    	setTimeout(function(){$("#nameError").modal("hide")},1600);
    })
	$('#nameError2').on('show.bs.modal', function () {
    	setTimeout(function(){$("#nameError2").modal("hide")},1600);
    })
	$('#phoneError').on('show.bs.modal', function () {
    	setTimeout(function(){$("#phoneError").modal("hide")},1600);
    })
	$('#passwordError').on('show.bs.modal', function () {
    	setTimeout(function(){$("#passwordError").modal("hide")},1600);
	})
	$('#passwordIsEmpty').on('show.bs.modal', function () {
    	setTimeout(function(){$("#passwordIsEmpty").modal("hide")},1600);
	})
	$('#passwordLengthError').on('show.bs.modal', function () {
    	setTimeout(function(){$("#passwordLengthError").modal("hide")},1600);
	})
	$('#passwordDifferError').on('show.bs.modal', function () {
    	setTimeout(function(){$("#passwordDifferError").modal("hide")},1600);
	})
});
