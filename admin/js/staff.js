/*
 * 作者：tzy
 * 日期：2019-04-13
 * */
var role="";
var department='';
var pageNum=1;
var rolemap ={
	"1":"经理",
	"2":"财务",
	"3":"总负责人",
	"4":"地区负责人",
	"5":"销售业务员",
	"6":"采购",
	"7":"地区操作员"
}
var username="";
var way=1;
//模态框
function alertStaffModal(){
	var modals=
	'<!--添加员工-->\
	<div class="modal fade" id="addStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">添加员工</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-6 margin-bottom-15">\
		<label for="name" class="control-label">* 姓名</label>\
		<input type="text" class="form-control" id="name" value="">\
	</div>\
	<div class="col-md-6 margin-bottom-15 pull-right">\
		<label for="sex" class="control-label">* 性别</label>\
		<select class="form-control margin-bottom-15" id="sex">\
			<option value="1">男</option>\
			<option value="0">女</option>\
		</select>\
	</div>\
	</div>\
	<div class="row">\
	<div class="col-md-6 margin-bottom-15 pull-right">\
		<label for="departMent" class="control-label">* 部门</label>\
		<select class="form-control margin-bottom-15" id="departMent">\
			<option>业务部</option>\
			<option>财务部</option>\
		</select>\
	</div>\
	<div class="col-md-6 margin-bottom-15 pull-right">\
		<label for="phone" class="control-label">* 电话</label>\
		<input type="text" class="form-control" id="phone" value="">\
	</div>\
	</div>\
	<div class="row">\
	<div class="col-md-6 margin-bottom-15">\
		<label for="role" class="control-label">* 角色</label>\
		<select class="form-control margin-bottom-15" placeholder="请选择" id="role">\
			<option value="1">经理</option>\
			<option value="2">财务</option>\
			<option value="3">总负责人</option>\
			<option value="4">地区负责人</option>\
			<option value="5">销售业务员</option>\
			<option value="6">采购</option>\
			<option value="7">地区操作员</option>\
		</select>\
	</div>\
	<div class="col-md-6 margin-bottom-15">\
		<label for="username" class="control-label">* 账号</label>\
		<input type="text" class="form-control" id="username" value="">\
	</div>\
	</div>\
	<div class="row">\
	<div class="col-md-6 margin-bottom-15">\
		<label for="password" class="control-label">* 密码</label>\
		<input type="password" class="form-control" id="password" value="">\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer">\
    <button type="button" class="btn btn-primary addStaffInfo">添加</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--批量导入-->\
	<div class="modal fade import" id="importStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">添加员工</h4>\
    </div>\
	<div class="modal-body">\
	<input type="file" name="file" class="btn btn-default"/>\
	<input type="button" class="btn btn-primary upLoadBtn" value="上传" data-toggle="modal" data-target="#inportSuccess"/>\
	<a class="downloadTemplet" href="#">点此下载模板</a>\
	</div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>';
	$('body').prepend(modals);
}

//构建表头
function buildTable() {
	var columns =[{"title":""},{"title":"员工姓名"},{"title":"部门"},{"title":"电话"},{"title":"角色"},{"title":""}];
	//给table加一个id 是二级目录的名字
	$('.mainTable').DataTable({
		responsive: true,
		searching: false,
		// "bSort":false,
		"columns":columns,
		"bLengthChange": false,
		"bRetrieve": false,
		"bFilter": false, //过滤功能
		"paging":false,
		"aLengthMenu":[15],
		"columnDefs": [ 
		{ "orderable": false, "targets": 1 },
		{ "orderable": false, "targets": 2 },
		{ "orderable": false, "targets": 3 },
		{ "orderable": false, "targets": 4 },
		{ "orderable": false, "targets": 5 },
		],
	});
}

//填充数据
function StaffTableDataAjax(role,department,num,type) {
	pageNum=num;
	var totalRecord='';
	if(type==1){
		$.ajax({
			type: "get",
			url: IP+"staff",
			data: {
				"pageNum":pageNum,
				"role":role,
				"departMent":department,
			},
			async: true,
			dataType: "json",
			contentType: "application/json",
			xhrFields: {withCredentials: true},
	      	crossDomain:true,
			success: function(obj) {
				console.log(obj);
				if(obj.code==200){
					setTableData(obj);
					totalRecord=obj.data.totalRecord;
					getPage(totalRecord,way);
				}
				if(obj.code==401){
					window.location.href = IP_ + 'login.html'
				}
			},
			Error: function() {
				alert("服务器出错");
			}
		})
	}
	if(type==2){
		$.ajax({
	      type: "GET",
	      url: IP+"staff/key",
	      data: {
	        "name": username,
			"pageNum": pageNum
	      },
	      async: true,
	      dataType: "json",
	      contentType: "application/json",
	      xhrFields: {withCredentials: true},
	      crossDomain:true,
	      success: function(obj) {
	        console.log(obj)
	        if(obj.code==200){
	        	setTableData(obj);
				totalRecord=obj.data.totalRecord;
				getPage(totalRecord,way);
	        }
	      },
	      Error: function() {
	        alert("服务器出错");
	      }
	    })
	}
}
//填充数据具体操作
function setTableData(obj) {
	$('#mainTable').dataTable().fnClearTable();
	$.each(obj.data.array, function(index, item) {
		order =(pageNum-1)*5+index+1;
		$('#mainTable').dataTable().fnAddData([
			'<span>'+order+'</span>',
			'<a href="staffdetail.html?id='+item.id+'" id="'+item.id+'">'+item.name+'</a>',
			'<span>'+item.departMent+'</span>',
			'<span>'+item.phone+'</span>',
			'<span>'+rolemap[item.role]+'</span>',
			'<a href="editstaff.html?id='+item.id+'" id="'+item.id+'"><button class="btn btn-info" style="margin-left:30px">编辑</button></a><button class="btn btn-warning btn_rePassword" style="margin-left:30px" data-toggle="modal" data-target="#resetPassword">重置</button>',
			])
	});
}

//分页操作
function getPage(totalRecord,way){
    layui.use(['laypage', 'layer'], function(){
  	var laypage = layui.laypage
  	,layer = layui.layer;
 		laypage.render({
           	elem: 'page',
           	count: totalRecord, //数据总数，从服务端得到
           	curr:pageNum,
			limit:5,
            //limits:[6,12,18,24],
			skip:true,
			first:"首页",
			last:"尾页",
			layout:['count','skip', 'prev', 'next'],  
            jump: function(obj, first){
                    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据
                    //console.log(obj.limit); //得到每页显示的条数
                    page=obj.curr;  //改变当前页码
                    imit=obj.limit;
                    //首次不执行
                    if(!first){
                    	pageNum=obj.curr;
		    		    StaffTableDataAjax(role,department,pageNum,way);
		    		}
                }
            
            });
        });
    }
//重置密码
$(document).on('click','.btn_rePassword',function(){
	var userid = $(this).parents('tr').children("td").eq(1).children("a").attr("id");
	console.log(userid)
	$('.temp').attr('id', userid);
})
$(document).on('click','.surePasswordYes',function(){
	var userid = $(this).parent('.temp').attr('id');
	$.ajax({
      type: "GET",
      url: IP+"password",
      data: {
        "id":userid
      },
      async: true,
      dataType: "json",
      contentType: "application/json",
      xhrFields: {withCredentials: true},
      crossDomain:true,
      success: function(obj) {
        console.log(obj)
        if(obj.code==200){
        	$('#resetPassword').modal('hide');
        	$('#newPassword').modal('show');
        	$('#password2').html(obj.data.pwd);

        }
      },
      Error: function() {
        alert("服务器出错");
      }
    })

});
//添加人员
function addStaffInfo(){
	$('.addStaffInfo').click(function(event) {
		var username = $('#username').val();
		var password = $('#password').val();
		var role = $("#role option:selected").val();
		var phone = $('#phone').val();
		var department = $('#departMent').val();
		var name = $('#name').val();
		console.log(username);
		if(username==""||password==""||role==""||phone==""||department==""||name==""){
			alert("还有信息未填写!");
		}else if(isPoneAvailable(phone)){
			alert("请填写正确的手机号码")
		}
		else if(password.length<6||password.length>16){
			alert("密码长度应为6位到16位!")
		}
		else{
			$.ajax({
				type: "POST",
				url: IP+"staff",
				data:JSON.stringify({
					"username":$('#username').val(),
					"password":$('#password').val(),
					"role":$("#role option:selected").val(), //角色
					"phone": $('#phone').val(),  //电话
					"department": $('#departMent').val(),//部门
					"name": $('#name').val(), //姓名
					"sex":$('#sex').val(),//性别
				}),
				async: true,
				dataType: "json",
				contentType: "application/json",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						/*StaffTableDataAjax("","",1,1);*/
						$('#addStaff').modal('hide');
						$('#addSuccess').modal('show');
						$("#addSuccess").on("hidden.bs.modal", function() {
							/*window.location.reload();*/
							StaffTableDataAjax("","",1,1);
						});
					}
					if (data.code==501) {
						$('#addStaff').modal('hide')
						$("#accountError").modal('show')
					}
					if (data.code==502) {
						$('#addStaff').modal('hide')
						$("#phoneError").modal('show')
					}
					if (data.code==503) {
						$('#addStaff').modal('hide')
						$("#nameError").modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
		
	});
}
//手机号码验证
function isPoneAvailable(str) {
    var myreg=/^[1][3,4,5,7,8,9][0-9]{9}$/;
    if (!myreg.test(str)) {
        return true;
    } else {
        return false;
    }
}

//搜索人员
function searchStaff(){
	$('#searchStaff').click(function(event) {
		pageNum=1;
		way=2;
		role="";
		department="";
		document.getElementById("rolelist").options.selectedIndex = 0; //回到初始状态
		document.getElementById("departentlist").options.selectedIndex = 0; //回到初始状态
		username = $("#searchbox").val();
		console.log(username)
		StaffTableDataAjax(role,department,pageNum,way);
		
		/*$.ajax({
	      type: "GET",
	      url: IP+"staff/key",
	      data: {
	        "name": username,
			"pageNum": pageNum
	      },
	      async: true,
	      dataType: "json",
	      contentType: "application/json",
	      xhrFields: {withCredentials: true},
	      crossDomain:true,
	      success: function(obj) {
	        console.log(obj)
	        if(obj.code==200){
	        	setTableData(obj);
				totalRecord=obj.data.totalRecord;
				getPage(totalRecord);
	        }
	      },
	      Error: function() {
	        alert("服务器出错");
	      }
	    })*/
	});
}
//筛选
function selectStaff(){
	$('#selectbtn').click(function(event) {
		pageNum=1;
		way=1;
		$('#searchbox').val("");  
		role = $("#rolelist option:selected").val();
		department = $("#departentlist option:selected").val();
		console.log(role);
		console.log(department);
		StaffTableDataAjax(role,department,pageNum,way);
	});
}
$(function() {
	buildTable();
	StaffTableDataAjax(role,department,pageNum,1);
	alertStaffModal();
	addStaffInfo();
	searchStaff();
	selectStaff();
	$("#addStaff").on("hidden.bs.modal", function() {
		$('input.form-control').val("");  
	});
})

