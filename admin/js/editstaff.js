(function (global) {
	var C1 = window.location.href.split("?")[1];
	var id = C1.split("=")[1];
	FillDetailAjax(id);

	function FillDetailAjax(id) {
		$.ajax({
			type: "get",
			url: IP + "staff/" + id,
			data: {},
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			async: true,
			dataType: "json",
			contentType: "application/json",
			success: function(obj) {
				if(obj.code==200){
					console.log(obj)
					fillDetail(obj)
				}
				if(obj.code==401){
					window.location.href = IP_ + 'login.html'
				}
			},
			Error: function() {
				alert("服务器出错");
			}
		})
	}
	
	function fillDetail(obj) {
		$("#name").val(obj.data.name);
		$("#phone").val(obj.data.phone);
		$(".role").html(rolemap[obj.data.role]);
		$("#departMent").val(obj.data.departMent);
		$(".username").html(obj.data.username);
		$("#sex").val(obj.data.sex);
	}
	
	global.upLoadData = function () {
		$.ajax({
			type: "PUT",
			url: IP+"staff",
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			data:JSON.stringify({
				"id":id,
				"name": $("#name").val(),
				"phone": $("#phone").val(),
				"department": $("#departMent").val(),
				"sex": parseInt($("#sex").val())
			}),
			async: true,
			dataType: "json",
			contentType: "application/json",
			success: function(obj) {
				console.log("修改后")
				console.log(obj)
				if(obj.code==200) {
					// if(id == $.cookie('id')){
					// 	$.cookie("nickname",$("#name").val(),{path:"/"});
					// }
					$('#saveSuccess').modal('show');
					$("#saveSuccess").on("hidden.bs.modal", function() {
						 window.location.href=document.referrer;    
					});
				} else if (obj.code==501) {
					$("#nameError").modal('show');
				} else if (obj.code==502) {
					$("#phoneError").modal('show')
				}
			},
			Error: function() {
				alert("服务器出错");
			}
		})
	}

	global.fillDetail = fillDetail;
})(window);