(function(global) {
	var id = $.cookie('id');

	function FillDetailAjax() {
		$.ajax({
			type: "get",
			url: IP + "staff/" + id,
			data: {},
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			async: true,
			dataType: "json",
			contentType: "application/json",
			success: function(obj) {
				if(obj.code==200){
					fillDetail(obj)
				}
				if(obj.code==401){
					window.location.href = IP_ + 'login.html'
				}
			},
			Error: function() {
				alert("服务器出错");
			}
		})
	}

	function fillDetail(obj) {
		$("#sex").val(obj.data.sex);
		$("#name").val(obj.data.name);
		$("#phone").val(obj.data.phone);
		$("#departMent").val(obj.data.departMent);
	}

	function upLoadData() {
		$.ajax({
			type: "PUT",
			url: IP + "staff",
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			data: JSON.stringify({
				"id": id,
				"name": $("#name").val(),
				"phone": $("#phone").val(),
				"department": $("#departMent").val(),
				"sex": parseInt($("#sex").val())
			}),
			async: true,
			dataType: "json",
			contentType: "application/json",
			success: function(obj) {
				if(obj.code == 200) {
					$.cookie("nickname",$("#name").val(),{path:"/"});
					$('#saveSuccess').modal('show');
					$("#saveSuccess").on("hidden.bs.modal", function() {
						window.location.href = document.referrer;
					});
				}
				if(obj.code == 501) {
					$("#nameError").modal('show');
				}
				if(obj.code == 502) {
					$("#phoneError").modal('show')
				}
			},
			Error: function() {
				alert("服务器出错");
			}
		})
	}
	FillDetailAjax();
	$("#yes").bind("click", function() {
		upLoadData();
	});
})(window);