$("#reset").bind("click",function(){
	var password1=$("#password").val();
	var password2=$("#repeatPassword").val();
	if(password1.length<6||password1.length>16){
		$('#passwordLengthError').modal('show');
	}else if(password1!=password2){
		$('#passwordDifferError').modal('show');
	}else{
		reset();
	}
  });
function reset(){
	$.ajax({
		type: "post",
		url: IP + "password" ,
		data:{
			"originPassword":$("#originPassword").val(),
			"password":$("#password").val()
		},
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		async: true,
		dataType: "json",
		success: function(obj) {
			console.log(obj)
			if(obj.code==200){
				$('#saveSuccess').modal('show');
				window.location.href = IP_ + 'login.html';
			}
			if (obj.code==500) {
				$("#passwordError").modal('show');	
			}
			if(obj.code==401){
				window.location.href = IP_ + 'login.html'
			}
		},
		Error: function() {
			alert("服务器出错");
		}
	})
}
