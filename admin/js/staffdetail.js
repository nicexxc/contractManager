(function (global) {
	var C1 = window.location.href.split("?")[1];
	var id = C1.split("=")[1];
	FillDetailAjax(id)
	$('.form-control').css('display', 'none');

	function FillDetailAjax(id) {
		$.ajax({
			type: "get",
			url: IP + "staff/" + id,
			data: {},
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			async: true,
			dataType: "json",
			contentType: "application/json",
			success: function(obj) {
				if(obj.code==200){
					console.log(obj)
					fillDetail(obj)
				}
				if(obj.code==401){
					window.location.href = IP_ + 'login.html'
				}
			},
			Error: function() {
				alert("服务器出错");
			}
		})
	}
	
	function fillDetail(obj) {
		$(".name").html(obj.data.name);
		$(".phone").html(obj.data.phone);
		$(".role").html(rolemap[obj.data.role]);
		$(".departMent").html(obj.data.departMent);
		$(".username").html(obj.data.username);
		$(".password").html(obj.data.password);
		$(".sex").html((obj.data.sex == 1?'男':'女'));
	}

	global.fillDetail = fillDetail;
})(window);