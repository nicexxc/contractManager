function FillDetailAjax(id,name) {
	$.ajax({
		type: "get",
		url: IP + "contentType",
		data: {
			"contractTypeId":id
		},
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		async: true,
		dataType: "json",
		contentType: "application/json",
		success: function(obj) {
			console.log(obj)
			if(obj.code==200){
				fillDetail(obj,name)
			}
			if(obj.code==401){
				window.location.href = IP_ + 'login.html'
			}
		},
		Error: function() {
			alert("服务器出错");
		}
	})
}
//构建表头
function buildTable() {
	var columns =[{"title":"编号"},{"title":"合同内容类别"},{"title":""}];
	//给table加一个id 是二级目录的名字
	$('.mainTable').DataTable({
		responsive: true,
		searching: false,
		// "bSort":false,
		"columns":columns,
		"bLengthChange": false,
		"bRetrieve": false,
		"bFilter": false, //过滤功能
		"paging":false,
		"aLengthMenu":[15],
		"columnDefs": [ 
		{ "orderable": false, "targets": 1 },
		{ "orderable": false, "targets": 2 },
		],
	});
}
function fillDetail(obj,name) {
	$(".type").html(name);
	console.log(obj.data)
	$('#mainTable').dataTable().fnClearTable();
	$.each(obj.data, function(index, item) {
		$('#mainTable').dataTable().fnAddData([
			'<span>'+item.id+'</span>',
			'<span>'+item.name+'</span>',
			'<button type="button" class="btn btn-warning btn_addContent" data-toggle="modal" data-target="#newContent" style="margin-left:30px">重命名</button>',
			])
	});

}
//模态框
function alertContractModal(){
	var modals=
	'<!--修改合同内容类别名称-->\
	<div class="modal fade" id="newContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">修改合同内容类别名称</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-3 margin-bottom-15" style="text-align:center">\
		<label for="name" class="control-label">合同内容类别名称:</label>\
	</div>\
	<div class="col-md-8 margin-bottom-15">\
		<input type="text" class="form-control" id="newContentname" value="">\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer temp">\
    <button type="button" class="btn btn-primary  newContent">确实</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>\
	<!--修改合同类别名称-->\
	<div class="modal fade" id="newtype" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
	<div class="modal-dialog">\
    <div class="modal-content">\
    <div class="modal-header">\
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
    <h4 class="modal-title" id="myModalLabel">修改合同类别名称</h4>\
    </div>\
	<div class="modal-body">\
	<div class="">\
	<div class="row">\
	<div class="col-md-3 margin-bottom-15" style="text-align:center">\
		<label for="name" class="control-label">合同类别名称:</label>\
	</div>\
	<div class="col-md-8 margin-bottom-15">\
		<input type="text" class="form-control" id="newtypename" value="">\
	</div>\
	</div>\
	</div>\
	</div>\
    <div class="modal-footer temp">\
    <button type="button" class="btn btn-primary  newtype">确实</button>\
    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
    </div>\
    </div><!-- /.modal-content -->\
    </div><!-- /.modal -->\
	</div>';
	$('body').prepend(modals);
}
$(document).on('click','.btn_addContent',function(){
	var userid = $(this).parents('tr').children("td").eq(0).children("span").text();
	console.log(userid)
	$('.temp').attr('id', userid);
})
$(function() {
	C1 = window.location.href.split("?")[1];
	var id = C1.split("=")[1].split("&")[0]
	C2 = decodeURI(window.location.href).split("&")[1];
	var name = C2.split("=")[1];
	console.log(name);
	console.log(id);
	buildTable();
	FillDetailAjax(id,name)
	alertContractModal()
	$('.newtype').click(function(event) {
		var newtypename = $('#newtypename').val();
		console.log(newtypename);
		if(newtypename==""){
			alert("还有信息未填写!");
		}else{
			$.ajax({
				type: "PUT",
				url: IP+"contractType",
				data:{
					"id": id,
					"typeName":newtypename
				},
				async: true,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						FillDetailAjax(id,newtypename)
						$('#newtype').modal('hide')
						$('#saveSuccess').modal('show')
					}
					if (data.code==501) {
						alert('无法修改');
						$('#newtype').modal('hide')
						$('#nameError2').modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
	
	});
	$('.newContent').click(function(event) {
		var newContentname = $('#newContentname').val();
		var userid = $(this).parent('.temp').attr('id');
		console.log(newContentname);
		if(newContentname==""){
			alert("还有信息未填写!");
		}else{
			$.ajax({
				type: "put",
				url: IP+"contentType",
				data:{
					"id": userid,
					"name":newContentname
				},
				async: true,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success: function(data) {
					console.log(data)
					if (data.code==200) {
						FillDetailAjax(id,name)
						$('#newContent').modal('hide')
						$('#saveSuccess').modal('show')
					}
					if (data.code==501) {
						$('#newContent').modal('hide')
						$('#nameError2').modal('show')
					}
				},
				Error: function() {
					alert("服务器出错");
				}
			})
		}
	
	});
	$("#newContent").on("hidden.bs.modal", function() {
		$('.form-control').val("");  
	});
	$("#newtype").on("hidden.bs.modal", function() {
		$('.form-control').val("");  
	});
})