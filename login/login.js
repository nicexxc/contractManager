(function () {
    $('#login').click(function () {
        var name = $('#username').val();
        var password = $('#password').val();
        if(name==""||password==""){
        	 pop('未填写完整！');
        }else{
	        $.ajax({
	            type:'post',
	            url:IP+'login',
	            data:{
	                username:name,
	                password:password
	            },
	            success:function (res) {
	                var code = res.code;
	                if(code == 501){
	                    pop('您已输错密码10次系统已为您重置密码请联系总部！',2000);
	                }else if(code == 502){
	                    pop('密码错误！');
	                }else if(code == 503){
	                    pop('用户名错误！');
	                }else if(code == 200){
						if(res.data.reset){
							window.location.href = IP_ + 'user/htmlPages/private.html'
						}else{
							var role = res.data.role;
							var name = res.data.nickname;
							var id = res.data.userId;
							var unread = res.data.unread;
							$.cookie('id',id);
							$.cookie('nickname',name,{path:"/"});
							$.cookie('role',role);
							$.cookie('unread',unread,{ path: '/' });
							if(role <= 3){
								window.location.href = IP_ + 'admin/html/staff.html'
							}else{
								if(role == 4){
									$.ajax({
										url:IP + 'staff/block',
										type:'GET',
										data:{},
										success:function (res) {
											if(res.data.blockId == 0){
												pop('请联系总部分配区块！',2000);
											}else{
												window.location.href = IP_ + 'user/htmlPages/contractList.html'
											}
										}
									});
								}else{
									window.location.href = IP_ + 'user/htmlPages/contractList.html'
								}
							}
						}
	                }else{
	
	                }
	            }
	        });
        }
        
    });
	$('#forget').click(function () {
	    pop('请联系总部重置密码！',2000);
	});
    function pop(msg,time) {
        if(!time){
            time = 1000
        }
        $('.pop-modal-msg').html(msg);
        $('.pop-modal').fadeIn(time);
        $('.pop-modal').fadeOut(time);
    }
})()