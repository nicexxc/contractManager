function header(){
	var header = '<div class="navbar navbar-inverse" role="navigation">\
  	<div class="navbar-header"><div class="logo"><img src="../images/logo.png"/><h1>合同管理系统</h1></div></div>\
  	<div class="navbar-right logo"><ul class="nav navbar-nav pull-right">\
  	<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>管理员</span>\
  	<span class="thumb-small"><img src="../images/admin.png"  class="img-circle"></span>\
  	<b class="caret"></b></a><ul class="dropdown-menu"><li><a href="info.html">查看个人信息</a></li><li><a href="login.html">退出</a></li></ul>\
 	</li></ul></div></div>'

	$(".topnav").html(header);
}
function footer() {
	var footer ='<div class="templatemo-copyright">\
	<p>地址：浙江省杭州市下沙高教园区学正街18号 <br>Copyright &copy;浙江工商大学ITObase</p>\
	</div>';
	$(".footer").html(footer);
}
$(function(){
	$.ajaxSetup({
        complete: function (xhr, status) {
            var d = eval('('+xhr.responseText+')');
            if(d.code == 401){
                alert(d.msg);
                window.location.href = IP_ + 'login.html'
            }else if(d.code == 403){
                alert(d.msg);
            }
        }
    });
	header();
	footer();
});
